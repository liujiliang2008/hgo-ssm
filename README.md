# Hgo敏捷开发脚手架SSM版

#### 项目介绍
最干净的敏捷开发脚手架，集权限、数据库双向同步、代码生成与一体，帮助用户快速开发项目

#### 软件架构
1. jdk：1.8
2. tomcat：tomcat8
3. 数据库：mysql
4. 前端：javaex
5. 后端：SSM
6. 权限：shiro

#### 开源协议：
	完全开源免费，无任何限制，可用于商业项目

#### 优势：
	将不是必要的功能以插件的形式剥离出来。
	基础源码只保留权限、数据库双向同步、代码生成，无垃圾代码，无冗余代码。
	定时任务、邮件发送等功能以插件的形式提供，用户可以自由插拔，不必担心版本的更新。

#### 安装教程
1. 系统演示：http://demo.javaex.cn/hgo/    账号：admin    密码：123456
2. 开发文档：http://nonfish.javaex.cn/doc/22
3. 官网：http://www.javaex.cn/
4. 视频教程：https://www.bilibili.com/video/av37256941/


#### 系统截图：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1202/115051_a1db5608_1712536.jpeg "2018-12-02_114747.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1202/115100_c13f583c_1712536.jpeg "2018-12-02_114801.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1202/115108_f553dcf4_1712536.jpeg "2018-12-02_114812.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1202/115118_4d00479d_1712536.jpeg "2018-12-02_114852.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1202/115125_4be25811_1712536.jpeg "2018-12-02_114920.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1202/115131_ee637d88_1712536.jpeg "2018-12-02_114932.jpg")
