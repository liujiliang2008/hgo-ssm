package #DAOPackage#;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import #EntityPackage#.#EntityClassName#;

public interface #DAOClassName# {
	
	/**
	 * 查询列表
	 * @param param
	 * @return
	 */
	List<#EntityClassName#> list(Map<String, Object> param);
	
	/**
	 * 根据主键，查询信息
	 * @param #ColumnIdPropertyName#
	 * @return
	 */
	#EntityClassName# selectById(String #ColumnIdPropertyName#);
	
	/**
	 * 插入
	 * @param #EntityObjectName#
	 * @return
	 */
	int insert(#EntityClassName# #EntityObjectName#);
	
	/**
	 * 更新
	 * @param #EntityObjectName#
	 * @return
	 */
	int update(#EntityClassName# #EntityObjectName#);
	
	/**
	 * 根据主键删除
	 * @param #ColumnIdPropertyName#
	 * @return
	 */
	int deleteById(String #ColumnIdPropertyName#);
	
	/**
	 * 批量删除
	 * @param #ColumnIdPropertyName#Arr
	 * @return
	 */
	int batchDelete(@Param("#ColumnIdPropertyName#Arr") String[] #ColumnIdPropertyName#Arr);

}
