<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>编辑#tableChsname#</title>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<!--面包屑导航-->
			<div class="content-header">
				<div class="breadcrumb">
					<span>菜单目录名称</span>
					<span class="divider">/</span>
					<span class="active">菜单名称</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div class="list-content">
				<!--块元素-->
				<div class="block">
					<!--修饰块元素名称-->
					<div class="banner">
						<p class="tab fixed">#tableChsname#</p>
					</div>
					<div class="main">
						<form id="form">
							<input type="hidden" name="#ColumnIdPropertyName#" value="${#EntityObjectName#.#ColumnIdPropertyName#}"/>
#JspEditFormFields#
							<!--提交按钮-->
							<div class="unit" style="width: 800px;">
								<div style="text-align: center;">
									<input type="button" onclick="goback()" class="button no" value="返回" />
									<input type="button" onclick="save()" class="button yes" value="保存" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	/**
	 * 保存
	 */
	function save() {
		// 表单验证函数
		if (javaexVerify()) {
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "save.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							window.location.href = document.referrer;
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 返回
	 */
	function goback() {
		history.back();
	}
</script>
</html>