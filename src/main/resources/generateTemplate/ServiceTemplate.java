package #ServicePackage#;

import java.util.List;
import java.util.Map;
#importUUID#
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import #DAOPackage#.#DAOClassName#;
import #EntityPackage#.#EntityClassName#;

@Service("#ServiceClassName#")
public class #ServiceClassName# {
	
	@Autowired
	private #DAOClassName# #DAOObjectName#;
	
	/**
	 * 查询列表
	 * @param param
	 * @return
	 */
	public List<#EntityClassName#> list(Map<String, Object> param) {
		return #DAOObjectName#.list(param);
	}
	
	/**
	 * 根据主键，查询信息
	 * @param id
	 * @return
	 */
	public #EntityClassName# selectById(String #ColumnIdPropertyName#) {
		return #DAOObjectName#.selectById(#ColumnIdPropertyName#);
	}
	
	/**
	 * 保存
	 * @param #EntityObjectName#
	 */
	public void save(#EntityClassName# #EntityObjectName#) {
		if (StringUtils.isEmpty(#EntityObjectName#.get#ColumnIdCapName#())) {
			// 新增#UUID#
			#DAOObjectName#.insert(#EntityObjectName#);
		} else {
			// 更新
			#DAOObjectName#.update(#EntityObjectName#);
		}
	}
	
	/**
	 * 根据主键删除
	 * @param #ColumnIdPropertyName#
	 */
	public void deleteById(String #ColumnIdPropertyName#) {
		#DAOObjectName#.deleteById(#ColumnIdPropertyName#);
	}
	
	/**
	 * 批量删除
	 * @param #ColumnIdPropertyName#Arr id数组
	 */
	public void batchDelete(String[] #ColumnIdPropertyName#Arr) {
		#DAOObjectName#.batchDelete(#ColumnIdPropertyName#Arr);
	}
}
