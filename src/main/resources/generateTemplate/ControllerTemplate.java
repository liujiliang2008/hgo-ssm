package #ControllerPackage#;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import #ServicePackage#.#ServiceClassName#;
import #EntityPackage#.#EntityClassName#;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("#TableName#")
public class #ControllerClassName# {
	
	@Autowired
	private #ServiceClassName# #ServiceObjectName#;
	
	/**
	 * 查询列表
	 * @param map
	 * @param keyword
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("list.action")
	public String list(ModelMap map,
			@RequestParam(required=false, value="keyword") String keyword,
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize) {
		
		Map<String, Object> param = new HashMap<String, Object>();
		if (StringUtils.isNotBlank(keyword)) {
			param.put("keyword", keyword.trim());
			map.put("keyword", keyword);
		}
		
		// pageHelper分页插件
		// 只需要在查询之前调用，传入当前页码，以及每一页显示多少条
		PageHelper.startPage(pageNum, pageSize);
		List<#EntityClassName#> list = #ServiceObjectName#.list(param);
		PageInfo<#EntityClassName#> pageInfo = new PageInfo<#EntityClassName#>(list);
		map.put("pageInfo", pageInfo);
		
		return "#jspPath#/#TableName#_list";
	}
	
	/**
	 * 添加/编辑
	 * @param map
	 * @param #ColumnIdPropertyName#
	 * @return
	 */
	@RequestMapping("edit.action")
	public String edit(ModelMap map, 
			@RequestParam(required=false, value="id") String #ColumnIdPropertyName#) {
		
		if (StringUtils.isNotEmpty(#ColumnIdPropertyName#)) {
			#EntityClassName# #EntityObjectName# = #ServiceObjectName#.selectById(#ColumnIdPropertyName#);
			map.put("#EntityObjectName#", #EntityObjectName#);
		}
		
		return "#jspPath#/#TableName#_edit";
	}
	
	/**
	 * 保存
	 * @param #EntityObjectName#
	 * @return
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public Result save(#EntityClassName# #EntityObjectName#) {
		
		#ServiceObjectName#.save(#EntityObjectName#);
		
		return Result.success();
	}
	
	/**
	 * 删除
	 * @param #ColumnIdPropertyName#
	 * @return
	 */
	@RequestMapping("delete.json")
	@ResponseBody
	public Result delete(@RequestParam(value="id") String #ColumnIdPropertyName#) {
		
		#ServiceObjectName#.deleteById(#ColumnIdPropertyName#);
		
		return Result.success();
	}
	
	/**
	 * 批量删除
	 * @param #ColumnIdPropertyName#Arr
	 * @return
	 */
	@RequestMapping("batchDelete.json")
	@ResponseBody
	public Result batchDelete(@RequestParam(value="idArr") String[] #ColumnIdPropertyName#Arr) {
		
		#ServiceObjectName#.batchDelete(#ColumnIdPropertyName#Arr);
		
		return Result.success();
	}
}
