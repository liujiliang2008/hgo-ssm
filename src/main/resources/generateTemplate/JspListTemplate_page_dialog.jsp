<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>#tableChsname#管理</title>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<!--面包屑导航-->
			<div class="content-header">
				<div class="breadcrumb">
					<span>菜单目录名称</span>
					<span class="divider">/</span>
					<span class="active">菜单名称</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div class="list-content">
				<div class="block">
					<h2>#tableChsname#列表</h2>
					<div class="main">
						<div class="admin-search">
							<div class="input-group">
								<input type="text" class="text" id="keyword" value="${keyword}" placeholder="关键词" autocomplete="off"/>
								<button class="button blue" onclick="search()">搜索</button>
							</div>
						</div>
						
						<div class="operation-wrap">
							<div class="buttons-wrap">
								<button class="button blue radius-3" onclick="edit()"><span class="icon-plus"></span> 添加</button>
								<button class="button red radius-3" onclick="batchDelete()"><span class="icon-remove"></span> 批量删除</button>
							</div>
						</div>
						<table id="table" class="table">
							<thead>
								<tr>
									<th class="checkbox"><input type="checkbox" class="fill listen-1" /> </th>
									<th style="width:40px;">序号</th>
#JspTableHeaders#
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${fn:length(pageInfo.list)==0}">
										<tr>
											<td colspan="#JspTableHeaderCount#" style="text-align:center;">暂无记录</td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach items="${pageInfo.list}" var="entity" varStatus="status" >
											<tr>
												<td><input type="checkbox" class="fill listen-1-2" name="id" value="${entity.#ColumnIdPropertyName#}" /> </td>
												<td>${status.count}</td>
#JspTableCells#
												<td>
													<button onclick="del(this, '${entity.#ColumnIdPropertyName#}')" class="button red radius-3"><span class="icon-trash-o"></span> 删除</button>
													<button onclick="edit('${entity.#ColumnIdPropertyName#}')" class="button indigo radius-3"><span class="icon-edit-2"></span> 编辑</button>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
						<!-- 分页 -->
						<div class="page">
							<ul id="page" class="pagination"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	var currentPage = "${pageInfo.pageNum}";
	var pageCount = "${pageInfo.pages}";
	javaex.page({
		id : "page",
		pageCount : pageCount,	// 总页数
		currentPage : currentPage,// 默认选中第几页
		// 返回当前选中的页数
		callback:function(rtn) {
			search(rtn.pageNum);
		}
	});
	
	/**
	 * 搜索
	 */
	function search(pageNum) {
		if (!pageNum) {
			pageNum = 1;
		}
		
		var keyword = $("#keyword").val();
		window.location.href = "list.action"
				+ "?keyword="+keyword
				+ "&pageNum="+pageNum
				;
	}
	
	/**
	 * 添加/编辑
	 */
	function edit(id) {
		var title = "编辑";
		if (!id) {
			id = "";
			title = "添加";
		}
		javaex.dialog({
			type : "window",
			title : title,
			url : "edit.action?id="+id,
			width : "800",
			height : "550",
			isBackground : true
		});
	}
	
	/**
	 * 删除
	 */
	function del(obj, id) {
		javaex.deleteDialog(
			obj,	// obj是必须的
			{
				content : "确定要删除么？",
				callback : "deleteCallback('"+id+"')"
			}
		);
	}
	function deleteCallback(id) {
		javaex.optTip({
			content : "删除中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "delete.json",
			type : "POST",
			dataType : "json",
			data : {
				"id" : id
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	/**
	 * 批量删除
	 */
	var idArr = new Array();
	function batchDelete() {
		idArr = [];
		$(':checkbox[name="id"]:checked').each(function() {
			idArr.push($(this).val());
		});
		
		// 判断至少选择一条记录
		if (idArr.length==0) {
			javaex.optTip({
				content : "至少选择一条记录",
				type : "error"
			});
			return;
		}
		
		javaex.confirm({
			content : "确定要删除么？",
			callback : "batchDeleteCallback()"
		});
	}
	function batchDeleteCallback() {
		javaex.optTip({
			content : "数据提交中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "batchDelete.json",
			type : "POST",
			dataType : "json",
			traditional : "true",
			data : {
				"idArr" : idArr
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
</script>
</html>
