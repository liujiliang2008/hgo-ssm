<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!--左侧内容-->
<div class="admin-left">
	<!--logo-->
	<div class="logo">
		<h1>${system_name}</h1>
	</div>
	
	<!--左侧菜单-->
	<div id="menu" class="menu">
		<ul>
			<c:choose>
				<c:when test="${fn:length(loginUserInfo.menuList)==0}">
					<li class="menu-item">
						<a href="javascript:;"><span class="icon-block"></span>没有权限</a>
					</li>
				</c:when>
				<c:otherwise>
					<c:forEach items="${loginUserInfo.menuList}" var="parent">
						<c:choose>
							<c:when test="${fn:length(parent.childrenList)==0}">
								<li class="menu-item">
									<a href="${domain}/${parent.url}"><span class="${parent.icon}"></span>${parent.name}</a>
								</li>
							</c:when>
							<c:otherwise>
								<li class="menu-item">
									<a href="javascript:;"><span class="${parent.icon}"></span>${parent.name}<i class="icon-keyboard_arrow_left"></i></a>
									<ul>
										<c:forEach items="${parent.childrenList}" var="child">
											<li>
												<a href="${domain}/${child.url}">
													<c:if test="${!empty child.icon}">
														<span class="${child.icon}" style="margin-right: 10px;"></span>
													</c:if>
													<span>${child.name}</span>
												</a>
											</li>
										</c:forEach>
									</ul>
								</li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</div>

<script>
	javaex.menu({
		id : "menu",
		isAutoSelected : true
	});
</script>