<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="admin-header">
	<ul class="header-right">
		<li>
			<c:choose>
				<c:when test="${empty loginUserInfo.avatar}">
					<a class="pull-left avatar" href="javascript:;"><img src="${domain}/static/plugin/javaex/pc/images/user.jpg"></a>
				</c:when>
				<c:otherwise>
					<a class="pull-left avatar" href="javascript:;"><img src="${loginUserInfo.avatar}"></a>
				</c:otherwise>
			</c:choose>
			<p class="pull-left margin-left-10">欢迎您，<span>${empty loginUserInfo.username?loginUserInfo.loginName:loginUserInfo.username}</span></p>
			<label class="margin-left-10 margin-right-10">|</label>
			<a href="${domain}/logout.action">退出</a>
		</li>
		<li>
			<a href="${domain}/hgo_user_info/userCenter.action">个人中心</a>
		</li>
	</ul>
</div>

<script>
	javaex.loading();
</script>