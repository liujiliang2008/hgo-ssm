<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>数据监控 - ${system_name}</title>
<style>
iframe {
	width: 100%;
	height: 100%;
	border: 0;
	margin: 0;
	padding: 0;
}
</style>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<!--面包屑导航-->
			<div class="content-header">
				<div class="breadcrumb">
					<span>系统监控</span>
					<span class="divider">/</span>
					<span class="active">数据监控</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div id="druid" class="list-content">
				<iframe src="${domain}/druid/index.html"></iframe>
			</div>
		</div>
	</div>
</body>
<script>
	// 设置最大高度
	var height = document.documentElement.clientHeight-151;
	$("#druid").height(height);
</script>
</html>