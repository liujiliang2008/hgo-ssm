<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>添加按钮 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main" style="margin-top:10px;">
		<form id="form">
			<!--父级菜单-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">父级菜单</p></div>
				<div class="right">
					<select id="parentId" name="parentId" disabled="disabled">
						<option value="${hgoMenuInfo.id}" selected>${hgoMenuInfo.name}</option>
					</select>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--菜单类型-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">类型</p></div>
				<div class="right">
					<ul class="equal-5">
						<li><input type="radio" class="fill" name="type" value="按钮" checked/>按钮</li>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--按钮名称-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">按钮名称</p></div>
				<div class="right">
					<input type="text" class="text" name="name" data-type="必填" value="" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--权限标识-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">权限标识</p></div>
				<div class="right">
					<input type="text" class="text" name="permCode" data-type="必填" value="" autocomplete="off"/>
					<p class="hint">格式：<font color="red">请求路径</font>。例如：hgo_menu_info/edit.action 或 hgo_menu_info/delete.json</p>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--排序-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">显示顺序</p></div>
				<div class="right">
					<input type="text" class="text" name="sort" data-type="正整数" error-msg="必须填写正整数" value="${maxSort}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--提交按钮-->
			<div class="unit iframe-button">
				<div style="text-align: center;">
					<input type="button" onclick="closeDialog()" class="button no" value="关闭" />
					<input type="button" onclick="save()" class="button yes" value="保存" />
				</div>
			</div>
		</form>
	</div>
</body>
<script>
	javaex.select({
		id : "parentId"
	});

	/**
	 * 保存
	 */
	function save() {
		if (javaexVerify()) {
			$("#parentId").attr("disabled", false);
			
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "save.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							// 关闭弹出层
							parent.javaex.close();
							// 刷新页面
							parent.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 关闭弹窗
	 */
	function closeDialog() {
		parent.javaex.close();
	}
</script>
</html>