<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>菜单管理 - ${system_name}</title>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<div class="content-header">
				<div class="breadcrumb">
					<span>权限配置</span>
					<span class="divider">/</span>
					<span class="active">菜单管理</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div class="list-content">
				<div class="block">
					<h2>菜单列表</h2>
					<div class="main">
						<p class="tip danger">菜单地址禁止以 / 开头</p>
						<div class="operation-wrap">
							<div class="buttons-wrap">
								<button class="button blue radius-3" onclick="addMenu()"><span class="icon-plus"></span> 添加</button>
								<button class="button green radius-3" onclick="saveSort()"><span class="icon-check2"></span> 更新排序</button>
							</div>
						</div>
						<table id="table" class="table">
							<thead>
								<tr>
									<th class="checkbox"><input type="radio" class="fill" name="radio" value=""/> </th>
									<th style="width:60px;">显示顺序</th>
									<th>菜单名称</th>
									<th>菜单地址</th>
									<th>菜单图标</th>
									<th>页面按钮权限</th>
									<th>类型</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list}" var="catalog">
									<tr id="tree_${catalog.id}" parentId="tree_${catalog.parentId}">
										<td><c:if test="${catalog.id!='1'}"><input type="radio" class="fill" name="radio" value="${catalog.id}"/> </c:if></td>
										<td>
											<input type="hidden" name="id" value="${catalog.id}" />
											<input type="text" class="text" name="sort" data-type="正整数" error-msg="必须输入正整数" value="${catalog.sort}" autocomplete="off"/>
										</td>
										<td>${catalog.name}</td>
										<td>${catalog.url}</td>
										<td><span class="${catalog.icon}" style="font-size: 18px;"></span></td>
										<td>${catalog.permCode}</td>
										<td>
											<c:choose>
												<c:when test="${catalog.type=='目录'}">
													<button class="button yellow empty radius-3" zoom="0.8">${catalog.type}</button>
												</c:when>
												<c:otherwise>
													<button class="button indigo empty radius-3" zoom="0.8">${catalog.type}</button>
												</c:otherwise>
											</c:choose>
										</td>
										<td>
											<c:if test="${catalog.isSystem!='1'}">
												<button onclick="deleteMenu(this, '${catalog.id}')" class="button red radius-3"><span class="icon-trash-o"></span> 删除</button>
												<button onclick="editMenu('${catalog.id}')" class="button indigo radius-3"><span class="icon-edit-2"></span> 编辑</button>
											</c:if>
										</td>
									</tr>
									<c:forEach items="${catalog.childrenList}" var="menu">
										<tr id="tree_${menu.id}" parentId="tree_${menu.parentId}">
											<td></td>
											<td>
												<input type="hidden" name="id" value="${menu.id}" />
												<input type="text" class="text" name="sort" data-type="正整数" error-msg="必须输入正整数" value="${menu.sort}" autocomplete="off"/>
											</td>
											<td>${menu.name}</td>
											<td>${menu.url}</td>
											<td><span class="${menu.icon}" style="font-size: 18px;"></span></td>
											<td>${menu.permCode}</td>
											<td><button class="button indigo empty radius-3" zoom="0.8">${menu.type}</button></td>
											<td>
												<c:if test="${menu.isSystem!='1'}">
													<button onclick="deleteMenu(this, '${menu.id}')" class="button red radius-3"><span class="icon-trash-o"></span> 删除</button>
													<button onclick="editMenu('${menu.id}')" class="button indigo radius-3"><span class="icon-edit-2"></span> 编辑</button>
												</c:if>
												<button onclick="addButton('${menu.id}')" class="button green radius-3"><span class="icon-beenhere"></span> 添加页面权限</button>
											</td>
										</tr>
										
										<c:forEach items="${menu.childrenList}" var="button">
											<tr id="tree_${button.id}" parentId="tree_${button.parentId}">
												<td></td>
												<td>
													<input type="hidden" name="id" value="${button.id}" />
													<input type="text" class="text" name="sort" data-type="正整数" error-msg="必须输入正整数" value="${button.sort}" autocomplete="off"/>
												</td>
												<td>${button.name}</td>
												<td>${button.url}</td>
												<td></td>
												<td>${button.permCode}</td>
												<td><button class="button green empty radius-3" zoom="0.8">${button.type}</button></td>
												<td>
													<c:if test="${button.isSystem!='1'}">
														<button onclick="deleteMenu(this, '${button.id}')" class="button red radius-3"><span class="icon-trash-o"></span> 删除</button>
														<button onclick="editButton('${button.id}')" class="button indigo radius-3"><span class="icon-edit-2"></span> 编辑</button>
													</c:if>
												</td>
											</tr>
										</c:forEach>
									</c:forEach>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	javaex.table({
		id : "table",
		tree : 3,
		isClose : true
	});
	
	/**
	 * 添加菜单
	 */
	function addMenu() {
		var id = $(':radio[name="radio"]:checked').val();
		if (!id) {
			id = "";
		}
		
		javaex.dialog({
			type : "window",
			title : "添加菜单",
			url : "addMenu.action?id="+id,
			width : "800",
			height : "550",
			isBackground : true
		});
	}
	
	/**
	 * 编辑菜单
	 */
	function editMenu(id) {
		javaex.dialog({
			type : "window",
			title : "编辑菜单",
			url : "editMenu.action?id="+id,
			width : "800",
			height : "550",
			isBackground : true
		});
	}
	
	/**
	 * 删除菜单
	 */
	function deleteMenu(obj, id) {
		javaex.deleteDialog(
			obj,	// obj是必须的
			{
				content : "确定要删除么？",
				callback : "deleteMenuCallback("+id+")"
			}
		);
	}
	function deleteMenuCallback(id) {
		javaex.optTip({
			content : "菜单删除中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "delete.json",
			type : "POST",
			dataType : "json",
			data : {
				"id" : id
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	/**
	 * 添加页面按钮权限
	 */
	function addButton(id) {
		javaex.dialog({
			type : "window",
			title : "添加按钮",
			url : "addButton.action?id="+id,
			width : "800",
			height : "550",
			isBackground : true
		});
	}
	
	/**
	 * 编辑页面按钮权限
	 */
	function editButton(id) {
		javaex.dialog({
			type : "window",
			title : "编辑按钮",
			url : "editButton.action?id="+id,
			width : "800",
			height : "550",
			isBackground : true
		});
	}
	
	/**
	 * 更新排序
	 */
	var idArr = new Array();
	var sortArr = new Array();
	function saveSort() {
		if (javaexVerify()) {
			idArr = [];
			sortArr = [];
			
			// id
			$('input[name="id"]').each(function() {
				idArr.push($(this).val());
			});
			
			// sort
			$('input[name="sort"]').each(function() {
				sortArr.push($(this).val());
			});
			
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "saveSort.json",
				type : "POST",
				dataType : "json",
				traditional : "true",
				data : {
					"idArr" : idArr,
					"sortArr" : sortArr
				},
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message
						});
						// 建议延迟加载
						setTimeout(function() {
							// 刷新页面
							window.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
</script>
</html>