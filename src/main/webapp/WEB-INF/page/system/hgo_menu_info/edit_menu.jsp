<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>编辑菜单 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main" style="margin-top:10px;">
		<form id="form">
			<input type="hidden" name="id" value="${hgoMenuInfo.id}"/>
			
			<!--父级菜单-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">父级菜单</p></div>
				<div class="right">
					<select id="parentId" name="parentId">
						<option value="-1">≡ 无 ≡</option>
						<c:forEach items="${parentList}" var="entity">
							<option value="${entity.id}" <c:if test="${entity.id=='1'}">disabled="disabled"</c:if> <c:if test="${entity.id==hgoMenuInfo.parentId}">selected</c:if>>${entity.name}</option>
						</c:forEach>
					</select>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--菜单类型-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">类型</p></div>
				<div class="right">
					<ul class="equal-5">
						<c:choose>
							<c:when test="${hgoMenuInfo.type=='目录'}">
								<li><input type="radio" class="fill" name="type" value="目录" checked/>目录</li>
								<li><input type="radio" class="fill" name="type" value="菜单"/>菜单</li>
							</c:when>
							<c:otherwise>
								<li><input type="radio" class="fill" name="type" value="目录"/>目录</li>
								<li><input type="radio" class="fill" name="type" value="菜单" checked/>菜单</li>
							</c:otherwise>
						</c:choose>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--菜单名称-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">菜单名称</p></div>
				<div class="right">
					<input type="text" class="text" name="name" data-type="必填" value="${hgoMenuInfo.name}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--菜单地址-->
			<div class="unit">
				<div class="left"><p class="subtitle">菜单地址</p></div>
				<div class="right">
					<input type="text" class="text" name="url" value="${hgoMenuInfo.url}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--排序-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">显示顺序</p></div>
				<div class="right">
					<input type="text" class="text" name="sort" data-type="正整数" error-msg="必须填写正整数" value="${hgoMenuInfo.sort}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--图标样式-->
			<div class="unit">
				<div class="left"><p class="subtitle">图标样式</p></div>
				<div class="right">
					<input type="text" class="text" name="icon" value="${hgoMenuInfo.icon}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--提交按钮-->
			<div class="unit iframe-button">
				<div style="text-align: center;">
					<input type="button" onclick="closeDialog()" class="button no" value="关闭" />
					<input type="button" onclick="save()" class="button yes" value="保存" />
				</div>
			</div>
		</form>
	</div>
</body>
<script>
	javaex.select({
		id : "parentId"
	});

	$(':radio[name="type"]').change(function() {
		format($(this).val());
	});
	
	/**
	 * 格式化数据
	 */
	function format(type) {
		if (type=="目录") {
			// 禁用菜单地址栏
			$('input[name="url"]').attr("readonly", true);
			$('input[name="url"]').addClass("readonly");
		} else {
			// 放开菜单地址栏
			$('input[name="url"]').attr("readonly", false);
			$('input[name="url"]').removeClass("readonly");
		}
	}
	
	/**
	 * 保存
	 */
	function save() {
		if (javaexVerify()) {
			$(':radio[name="type"]').attr("disabled", false);
			
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "save.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							// 关闭弹出层
							parent.javaex.close();
							// 刷新页面
							parent.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 关闭弹窗
	 */
	function closeDialog() {
		parent.javaex.close();
	}
</script>
</html>