<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>角色管理 - ${system_name}</title>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<div class="content-header">
				<div class="breadcrumb">
					<span>权限配置</span>
					<span class="divider">/</span>
					<span class="active">角色管理</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div class="list-content">
				<div class="grid-3-2 spacing-20">
					<div>
						<div class="block">
							<h2>角色列表</h2>
							<div class="main">
								<div class="operation-wrap">
									<div class="buttons-wrap">
										<button class="button blue radius-3" onclick="edit()"><span class="icon-plus"></span> 添加</button>
									</div>
								</div>
								<table class="table">
									<thead>
										<tr>
											<th></th>
											<th>角色标识</th>
											<th>角色名称</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${pageInfo.list}" var="entity" varStatus="status">
											<tr>
												<td>${status.count}</td>
												<td>${entity.code}</td>
												<td>${entity.name}</td>
												<td>
													<button onclick="del(this, '${entity.id}')" class="button red radius-3"><span class="icon-trash-o"></span> 删除</button>
													<button onclick="edit('${entity.id}')" class="button indigo radius-3"><span class="icon-edit-2"></span> 编辑</button>
													<button onclick="editUser('${entity.id}', '${entity.name}')" class="button green radius-3"><span class="icon-user"></span> 用户配置</button>
													<button onclick="editPerm('${entity.id}', '${entity.name}')" class="button yellow radius-3"><span class="icon-beenhere"></span> 权限配置</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<!-- 分页 -->
								<div class="page">
									<ul id="role_page" class="pagination"></ul>
								</div>
							</div>
						</div>
					</div>
					<div>
						<div id="default" class="block">
							<h2>权限设置</h2>
						</div>
						<div id="user" class="block" style="display:none;">
							<h2>用户设置：<span id="role_user_name"></span></h2>
							<div class="main">
								<div class="operation-wrap">
									<div class="buttons-wrap">
										<button class="button blue radius-3" onclick="addRoleUser()"><span class="icon-plus"></span> 添加</button>
										<button class="button red radius-3" onclick="batchDeleteUser()"><span class="icon-plus"></span> 批量移除</button>
									</div>
								</div>
								<table id="table" class="table">
									<thead>
										<tr>
											<th class="checkbox"><input type="checkbox" class="fill listen-1" /> </th>
											<th>序号</th>
											<th>账号</th>
											<th>姓名</th>
											<th>操作</th>
										</tr>
									</thead>
								</table>
								<!-- 分页 -->
								<div class="page">
									<ul id="user_page" class="pagination"></ul>
								</div>
							</div>
						</div>
						<div id="role" class="block" style="overflow: auto;display:none;">
							<h2>菜单设置：<span id="role_menu_name"></span></h2>
							<button onclick="savePermMenu()" class="button green radius-3" style="position: absolute;right: 20px;top: 16px;"><span class="icon-save-disk"></span> 保存</button>
							<div class="main" style="overflow: auto;">
								<div id="tree" class="tree"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script>
	// 设置最大高度，菜单太多时，显示滚动条
	var maxHeight = document.documentElement.clientHeight-252;
	$("#role .main").css({
		maxHeight : maxHeight + "px"
	});
	
	var currentPage = "${pageInfo.pageNum}";
	var pageCount = "${pageInfo.pages}";
	javaex.page({
		id : "role_page",
		pageCount : pageCount,	// 总页数
		currentPage : currentPage,// 默认选中第几页
		// 返回当前选中的页数
		callback:function(rtn) {
			window.location.href = "list.action?pageNum="+rtn.pageNum;
		}
	});
	
	/**
	 * 角色编辑
	 */
	function edit(id) {
		var title = "编辑角色";
		if (!id) {
			id = "";
			title = "添加角色";
		}
		
		javaex.dialog({
			type : "window",
			title : title,
			url : "edit.action?id="+id,
			width : "800",
			height : "550",
			isBackground : true
		});
	}
	
	/**
	 * 删除角色
	 */
	function del(obj, id) {
		javaex.deleteDialog(
			obj,	// obj是必须的
			{
				content : "确定要删除么？",
				callback : "deleteCallback("+id+")"
			}
		);
	}
	
	function deleteCallback(id) {
		javaex.optTip({
			content : "删除中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "delete.json",
			type : "POST",
			dataType : "json",
			data : {
				"id" : id
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	var roleId = "";
	//==========↓↓↓↓↓权限配置↓↓↓↓↓==========//
	var menuIdArr = new Array();
	function editPerm(id, roleName) {
		$("#default").hide();
		$("#role").show();
		$("#user").hide();
		
		$("#role_menu_name").text(roleName);
		roleId = id;
		menuIdArr = [];
		
		// 预加载
		javaex.loading({
			containerId : "tree",
			type : "open",
			top : "120px"
		});
		
		$.ajax({
			url : "${domain}/hgo_menu_info/listMenuByRole.json",
			type : "POST",
			dataType : "json",
			data : {
				"roleId" : roleId
			},
			success : function(rtn) {
				javaex.loading({
					type : "close"
				});
				
				if (rtn.code=="000000") {
					$("#tree").empty();
					
					var list = rtn.data.list;
					javaex.tree({
						id : "tree",
						data : list,
						type : 1,				// 数据结构类型（1或2）
						isShowAllSelect : true,	// 是否显示全选
						icon : false,			// 是否显示图标
						checkbox : true,		// 是否显示复选框
						callback : function(rtn) {
							menuIdArr = rtn.idArr;
						}
					});
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	/**
	 * 保存角色的权限配置
	 */
	function savePermMenu() {
		javaex.optTip({
			content : "数据提交中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "${domain}/hgo_role_menu/save.json",
			type : "POST",
			dataType : "json",
			traditional : "true",
			data : {
				"roleId" : roleId,
				"menuIdArr" : menuIdArr
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	//==========↑↑↑↑↑权限配置↑↑↑↑↑==========//
	
	//==========↓↓↓↓↓用户配置↓↓↓↓↓==========//
	function editUser(id, roleName) {
		$("#default").hide();
		$("#role").hide();
		$("#user").show();
		
		roleId = id;
		if (!!roleName) {
			$("#role_user_name").text(roleName);
		}
		
		loadRoleUser(roleId, 1);
	}
	/**
	 * 加载该角色的用户数据
	 */
	function loadRoleUser(roleId, pageNum) {
		var tableId = "table";
		// 预加载
		javaex.loading({
			containerId : tableId,
			type : "open",
			top : "180px"
		});
		
		$.ajax({
			url : "${domain}/hgo_user_info/listUserByRole.json",
			type : "POST",
			dataType : "json",
			data : {
				"roleId" : roleId,
				"pageNum" : pageNum
			},
			success : function(rtn) {
				javaex.loading({
					containerId : tableId,
					type : "close"
				});
				
				if (rtn.code=="000000") {
					var pageInfo = rtn.data.pageInfo;
					var list = pageInfo.list;
					
					jsonTable(tableId, list);
					$("#user_page").empty();
					javaex.page({
						id : "user_page",
						pageCount : pageInfo.pages,	// 总页数
						currentPage : pageInfo.pageNum,// 默认选中第几页
						callback : function(rtn) {
							loadRoleUser(roleId, rtn.pageNum)
						}
					});
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	/**
	 * 异步加载表格数据
	 */
	function jsonTable(tableId, dataArr) {
		var html = '';
		html += '<tbody>';
		if (dataArr.length>0) {
			for (var i=0; i<dataArr.length; i++) {
				html += '<tr>';
				html += '	<td class="checkbox"><input type="checkbox" class="fill listen-1-2" name="'+tableId+'-userId" value="'+dataArr[i].id+'"/> </td>';
				html += '	<td>'+(i+1)+'</th>';
				html += '	<td>'+dataArr[i].loginName+'</th>';
				html += '	<td>'+dataArr[i].username+'</th>';
				html += '	<td><button onclick="deleteUser(this, \''+dataArr[i].id+'\')" class="button red radius-3"><span class="icon-trash-o"></span> 移除</button></th>';
				html += '</tr>';
			}
		} else {
			html += '<tr><td colspan="5" style="text-align:center;">暂无记录</td></tr>';
		}
		html += '</tbody>';
		
		$("#"+tableId+" tbody").empty();
		$("#"+tableId).append(html);
		
		// 重新渲染
		javaex.render();
	}
	
	/**
	 * 为指定角色添加用户
	 */
	function addRoleUser() {
		javaex.dialog({
			type : "window",
			title : "添加用户",
			url : "${domain}/hgo_user_role/addUser.action?roleId="+roleId,
			width : "1000",
			height : "650",
			isBackground : true,
			isClickMaskClose : true
		});
	}
	
	/**
	 * 移除用户
	 */
	function deleteUser(obj, userId) {
		javaex.deleteDialog(
			obj,	// obj是必须的
			{
				content : "确定移除该用户么？",
				callback : "callbackDeleteUser('"+userId+"')"
			}
		);
	}
	function callbackDeleteUser(userId) {
		javaex.optTip({
			content : "数据提交中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "${domain}/hgo_user_role/deleteUser.json",
			type : "POST",
			dataType : "json",
			data : {
				"roleId" : roleId,
				"userId" : userId
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					editUser(roleId);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	/**
	 * 批量移除用户
	 */
	var userIdArr = new Array();
	function batchDeleteUser() {
		userIdArr = [];
		$(':checkbox[name="table-userId"]:checked').each(function() {
			userIdArr.push($(this).val());
		});
		
		// 判断至少选择一条记录
		if (userIdArr.length==0) {
			javaex.optTip({
				content : "至少选择一条记录",
				type : "error"
			});
			return;
		}
		
		javaex.confirm({
			content : "移除这些用户？",
			callback : "callbackBatchDeleteUser()"
		});
	}
	function callbackBatchDeleteUser() {
		javaex.optTip({
			content : "数据提交中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "${domain}/hgo_user_role/batchDeleteUser.json",
			type : "POST",
			dataType : "json",
			traditional : "true",
			data : {
				"roleId" : roleId,
				"userIdArr" : userIdArr
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					editUser(roleId);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	//==========↑↑↑↑↑用户配置↑↑↑↑↑==========//
</script>
</html>