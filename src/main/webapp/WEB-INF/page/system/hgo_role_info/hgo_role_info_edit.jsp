<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>编辑角色 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main" style="margin-top:10px;">
		<form id="form">
			<input type="hidden" name="id" value="${hgoRoleInfo.id}"/>
			<!--角色名称-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">角色名称</p></div>
				<div class="right">
					<input type="text" class="text" name="name" data-type="必填" value="${hgoRoleInfo.name}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--角色标识-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">角色标识</p></div>
				<div class="right">
					<input type="text" class="text" name="code" data-type="必填" value="${hgoRoleInfo.code}" autocomplete="off"/>
					<p class="hint">不可重复</p>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--排序-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">显示排序</p></div>
				<div class="right">
					<input type="text" class="text" name="sort" data-type="正整数" error-msg="必须填写正整数" value="${empty hgoRoleInfo.sort?maxSort:hgoRoleInfo.sort}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--提交按钮-->
			<div class="unit iframe-button">
				<div style="text-align: center;">
					<input type="button" onclick="closeDialog()" class="button no" value="关闭" />
					<input type="button" onclick="save()" class="button yes" value="保存" />
				</div>
			</div>
		</form>
	</div>
</body>
<script>
	/**
	 * 保存
	 */
	function save() {
		if (javaexVerify()) {
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "save.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							// 关闭弹出层
							parent.javaex.close();
							// 刷新页面
							parent.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 关闭弹窗
	 */
	function closeDialog() {
		parent.javaex.close();
	}
</script>
</html>