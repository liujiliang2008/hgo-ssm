<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>为当前角色添加用户 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main-10">
		<div class="operation-wrap">
			<div class="buttons-wrap">
				<button class="button blue radius-3" href="javascript:;" onclick="addUser()"><span class="icon-plus"></span> 添加</button>
				
				<div class="input-group" style="float: right;">
					<input type="text" class="text" id="keyword" value="${keyword}" placeholder="账号或姓名" autocomplete="off"/>
					<button class="button blue radius-3" onclick="search()">搜索</button>
				</div>
			</div>
		</div>
		<table id="table" class="table">
			<thead>
				<tr>
					<th class="checkbox"><input type="checkbox" class="fill listen-1" /> </th>
					<th>序号</th>
					<th>账号</th>
					<th>姓名</th>
					<th>状态</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${fn:length(pageInfo.list)==0}">
						<tr>
							<td colspan="5" style="text-align:center;">暂无记录</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${pageInfo.list}" var="entity" varStatus="status" >
							<tr>
								<td class="checkbox"><input type="checkbox" class="fill listen-1-2" value="${entity.id}" name="userId" /> </td>
								<td>${status.count}</td>
								<td>${entity.loginName}</td>
								<td>${entity.username}</td>
								<td>
									<c:choose>
										<c:when test="${entity.status=='1'}">
											<font color="green">正常</font>
										</c:when>
										<c:otherwise>
											<font color="red">禁止登录</font>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<!-- 分页 -->
		<div class="page">
			<ul id="page" class="pagination"></ul>
		</div>
	</div>
</body>
<script>
	var roleId = "${roleId}";
	var currentPage = "${pageInfo.pageNum}";
	var pageCount = "${pageInfo.pages}";
	
	javaex.page({
		id : "page",
		pageCount : pageCount,	// 总页数
		currentPage : currentPage,// 默认选中第几页
		// 返回当前选中的页数
		callback:function(rtn) {
			search(rtn.pageNum);
		}
	});

	/**
	 * 搜索
	 */
	function search(pageNum) {
		if (!pageNum) {
			pageNum = 1;
		}
		
		var keyword = $("#keyword").val();
		window.location.href = "${domain}/hgo_user_role/addUser.action"
				+ "?roleId="+roleId
				+ "&keyword="+keyword
				+ "&pageNum="+pageNum
				;
	}
	
	/**
	 * 添加用户
	 */
	function addUser() {
		var userIdArr = new Array();
		$(':checkbox[name="userId"]:checked').each(function() {
			userIdArr.push($(this).val());
		});
		
		// 判断至少选择一条记录
		if (userIdArr.length==0) {
			javaex.optTip({
				content : "至少选择一条记录",
				type : "error"
			});
			return;
		}
		
		javaex.optTip({
			content : "数据提交中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "${domain}/hgo_user_role/save.json",
			type : "POST",
			dataType : "json",
			traditional : "true",
			data : {
				"roleId" : roleId,
				"userIdArr" : userIdArr
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
						// 刷新父窗口用户数据
						parent.editUser(roleId);
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
</script>
</html>