<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>编辑用户 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main" style="margin-top:10px;">
		<form id="form">
			<input type="hidden" name="id" value="${hgoUserInfo.id}"/>
			<!--账号-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">账号</p></div>
				<div class="right">
					<c:choose>
						<c:when test="${empty hgoUserInfo.loginName}">
							<input type="text" class="text" name="loginName" data-type="必填" value="" autocomplete="off"/>
						</c:when>
						<c:otherwise>
							<input type="text" class="text readonly" name="loginName" value="${hgoUserInfo.loginName}" readonly/>
						</c:otherwise>
					</c:choose>
					<p class="hint">编辑时，账号不可修改</p>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--密码-->
			<div class="unit">
				<div class="left">
					<c:if test="${empty hgoUserInfo.password}"><span class="required">*</span></c:if><p class="subtitle">密码</p>
				</div>
				<div class="right">
					<c:choose>
						<c:when test="${empty hgoUserInfo.password}">
							<input type="text" class="text" name="password" data-type="必填" value="${hgoUserInfo.password}" autocomplete="off"/>
						</c:when>
						<c:otherwise>
							<input type="text" class="text" name="password" value="" autocomplete="off"/>
						</c:otherwise>
					</c:choose>
					<p class="hint">编辑时，留空表示不修改密码</p>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--姓名-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">姓名</p></div>
				<div class="right">
					<input type="text" class="text" name="username" data-type="必填" value="${hgoUserInfo.username}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--邮箱-->
			<div class="unit">
				<div class="left"><p class="subtitle">邮箱</p></div>
				<div class="right">
					<input type="text" class="text" name="email" data-type="空|邮箱" error-msg="邮箱格式错误" value="${hgoUserInfo.email}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--手机号-->
			<div class="unit">
				<div class="left"><p class="subtitle">手机号</p></div>
				<div class="right">
					<input type="text" class="text" name="phone" data-type="空|手机号" error-msg="手机号格式错误" value="${hgoUserInfo.phone}" autocomplete="off"/>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--状态-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">状态</p></div>
				<div class="right">
					<ul class="equal-5">
						<c:choose>
							<c:when test="${hgoUserInfo.status=='2'}">
								<li><input type="radio" class="fill" name="status" value="1" />正常</li>
								<li><input type="radio" class="fill" name="status" value="2" checked/>禁止登录</li>
							</c:when>
							<c:otherwise>
								<li><input type="radio" class="fill" name="status" value="1" checked/>正常</li>
								<li><input type="radio" class="fill" name="status" value="2" />禁止登录</li>
							</c:otherwise>
						</c:choose>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--提交按钮-->
			<div class="unit iframe-button">
				<div style="text-align: center;">
					<input type="button" onclick="closeDialog()" class="button no" value="关闭" />
					<input type="button" onclick="save()" class="button yes" value="保存" />
				</div>
			</div>
		</form>
	</div>
</body>
<script>
	/**
	 * 保存
	 */
	function save() {
		if (javaexVerify()) {
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "save.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							// 关闭弹出层
							parent.javaex.close();
							// 刷新页面
							parent.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 关闭弹窗
	 */
	function closeDialog() {
		parent.javaex.close();
	}
</script>
</html>