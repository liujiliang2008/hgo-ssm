<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>修改密码 - ${system_name}</title>
</head>
<body>
	<!--块元素-->
	<div class="block">
		<!--块元素标题-->
		<div class="banner">
			<p class="tab fixed">修改密码</p>
		</div>
		<!--正文内容-->
		<div class="main">
			<form id="form">
				<!--原密码-->
				<div class="unit">
					<div class="left"><span class="required">*</span><p class="subtitle">原密码</p></div>
					<div class="right">
						<input type="text" class="text" name="oldPassword" data-type="必填" placeholder="请输入原密码" autocomplete="off" />
					</div>
					<span class="clearfix"></span>
				</div>
				
				<!--新密码-->
				<div class="unit">
					<div class="left"><span class="required">*</span><p class="subtitle">新密码</p></div>
					<div class="right">
						<input type="password" class="text" id="newPassword1" name="newPassword" data-type="必填" placeholder="请输入新密码" autocomplete="off" />
					</div>
					<span class="clearfix"></span>
				</div>
				
				<!--新密码-->
				<div class="unit">
					<div class="left"><span class="required">*</span><p class="subtitle">再输一遍</p></div>
					<div class="right">
						<input type="password" class="text" id="newPassword2" data-type="必填" placeholder="请再次输入新密码" autocomplete="off" />
					</div>
					<span class="clearfix"></span>
				</div>
				
				<!--提交按钮-->
				<div class="unit">
					<div style="text-align: center;">
						<input type="button" onclick="closeDialog()" class="button no" value="关闭" />
						<input type="button" onclick="save()" class="button yes" value="保存" />
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<script>
	/**
	 * 保存
	 */
	function save() {
		if (javaexVerify()) {
			var newPassword1 = $("#newPassword1").val();
			var newPassword2 = $("#newPassword2").val();
			if (newPassword1!=newPassword2) {
				javaex.optTip({
					content : "2次输入的新密码不一致",
					type : "error"
				});
				return;
			}
			
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "savePassword.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							// 关闭弹出层
							parent.javaex.close();
							// 刷新页面
							parent.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 关闭弹窗
	 */
	function closeDialog() {
		parent.javaex.close();
	}
</script>
</html>