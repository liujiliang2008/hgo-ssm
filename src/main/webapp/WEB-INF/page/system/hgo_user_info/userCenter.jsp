<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>个人中心 - ${system_name}</title>
<style>
#img-avatar-personal {
	width: 100px;
	height: 100px;
	border-radius: 50px;
	margin: 0;
	padding: 0;
	display: block;
	cursor: pointer;
	-webkit-transition: .2s 0s box-shadow ease;
	-moz-transition: .2s 0s box-shadow ease;
	-ms-transition: .2s 0s box-shadow ease;
	-o-transition: .2s 0s box-shadow ease;
	transition: .2s 0s box-shadow ease;
	margin: 0;
	border: 1px solid #fff;
	box-shadow: 0 1px 3px rgba(0,0,0,0);
	font-size: 12px;
	line-height: 16px;
	overflow: hidden;
	background-color: #eee;
}
</style>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<div class="content-header">
				<div class="breadcrumb">
					<span>个人中心</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div class="list-content">
				<div class="block">
					<!--banner用来修饰块元素的名称-->
					<div class="banner">
						<p class="tab fixed">个人中心</p>
					</div>
					<!--正文内容-->
					<div class="main">
						<div class="grid-1-9">
							<div>
								<a href="javascript:;" style="display: inline-block;">
									<c:choose>
										<c:when test="${empty loginUserInfo.avatar}">
											<img id="img-avatar-personal" src="${domain}/static/plugin/javaex/pc/images/user.jpg">
										</c:when>
										<c:otherwise>
											<img id="img-avatar-personal" src="${loginUserInfo.avatar}">
										</c:otherwise>
									</c:choose>
								</a>
							</div>
							<div>
								<div class="unit">
									<div class="left"><p class="subtitle">登录名</p></div>
									<div class="right">
										${loginUserInfo.loginName}
									</div>
									<span class="clearfix"></span>
								</div>
								<p class="divider"></p>
								<div class="unit">
									<div class="left"><p class="subtitle">姓名</p></div>
									<div class="right">
										${loginUserInfo.username}
									</div>
									<span class="clearfix"></span>
								</div>
								<p class="divider"></p>
								<div class="unit">
									<div class="left"><p class="subtitle">邮箱</p></div>
									<div class="right">
										${loginUserInfo.email}
									</div>
									<span class="clearfix"></span>
								</div>
							</div>
						</div>
						<p class="divider"></p>
						<div>
							<button class="button navy radius-3" onclick="changeAvatar()"><span class="icon-user"></span> 修改头像</button>
							<button class="button navy radius-3" onclick="changePassword()" style="float: right;"><span class="icon-lock2"></span> 修改密码</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	/**
	 * 修改头像
	 */
	function changeAvatar() {
		javaex.dialog({
			type : "login",	// 弹出层类型
			width : "900",
			height : "630",
			url : "changeAvatar.action"
		});
	}
	
	/**
	 * 修改密码
	 */
	function changePassword() {
		javaex.dialog({
			type : "login",	// 弹出层类型
			width : "560",
			height : "330",
			url : "changePassword.action"
		});
	}
</script>
</html>
