<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>导入数据 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main" style="margin-top:10px;">
		<p class="tip danger">必须使用2007以上版本的Excel，即后缀是.xlsx</p>
		
		<div class="unit">
			<div class="left"><p class="subtitle">导入方式</p></div>
			<div class="right">
				<ul class="equal-4">
					<li><input type="radio" class="fill" name="type" value="1" checked/>增量导入</li>
					<li><input type="radio" class="fill" name="type" value="0"/>更新导入</li>
					<span class="clearfix"></span>
				</ul>
			</div>
			<span class="clearfix"></span>
		</div>
		<p class="tip">
			增量导入：跳过相同<br />
			更新导入：相同数据更新
		</p>
		
		<div style="text-align: right;margin-bottom:15px;">
			<a href="javascript:;" class="file-container button blue radius-3">
				<span class="icon-cloud_upload"></span> 导入演示<input type="file" class="file" id="upload" />
			</a>
		</div>
		
		<!-- 返回的信息 -->
		<div id="errorMsg"></div>
	</div>
</body>
<script>
	/**
	 * 导入
	 */
	javaex.upload({
		type : "file",
		url : "${domain}/hgo_poi_demo/importExcel.json",	// 请求路径
		id : "upload",			// <input type="file" />的id
		param : "file",			// 参数名称，SSM中与MultipartFile的参数名保持一致
		callback : function (rtn) {
			if (rtn.code=="000000") {
				$("#errorMsg").append(rtn.data.errorMsg);
			} else {
				javaex.optTip({
					content : rtn.message,
					type : "error"
				});
			}
		}
	});
</script>
</html>