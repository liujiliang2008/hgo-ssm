<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>用户管理 - ${system_name}</title>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<div class="content-header">
				<div class="breadcrumb">
					<span>权限配置</span>
					<span class="divider">/</span>
					<span class="active">用户管理</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div class="list-content">
				<div class="block">
					<h2>用户列表</h2>
					<div class="main">
						<div class="admin-search">
							<span><input type="checkbox" class="fill" name="status" value="2" <c:if test="${status=='2'}">checked</c:if>/>禁止登录</span>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<div class="input-group">
								<input type="text" class="text" id="keyword" value="${keyword}" placeholder="账号或姓名" autocomplete="off"/>
								<button class="button blue" onclick="search()">搜索</button>
							</div>
						</div>
						
						<div class="operation-wrap">
							<div class="buttons-wrap">
								<button class="button blue radius-3" onclick="edit()"><span class="icon-plus"></span> 添加</button>
								<a href="${domain}/hgo_poi_demo/exportExcel.action" class="button green radius-3"><span class="icon-cloud_download"></span> 导出演示</a>
								<a href="${domain}/hgo_poi_demo/exportTemplate.action" class="button green radius-3"><span class="icon-cloud_download"></span> 下载导入模板演示</a>
								<button class="button green radius-3" onclick="importExcel()"><span class="icon-cloud_upload"></span> 导入演示</button>
							</div>
						</div>
						<table id="table" class="table">
							<thead>
								<tr>
									<th class="checkbox"></th>
									<th>账号</th>
									<th>姓名</th>
									<th>邮箱</th>
									<th>手机号</th>
									<th>状态</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${pageInfo.list}" var="entity" varStatus="status" >
									<tr>
										<td>${status.count}</td>
										<td>${entity.loginName}</td>
										<td>${entity.username}</td>
										<td>${entity.email}</td>
										<td>${entity.phone}</td>
										<td>
											<c:choose>
												<c:when test="${entity.status=='1'}">
													<font color="green">正常</font>
												</c:when>
												<c:otherwise>
													<font color="red">禁止登录</font>
												</c:otherwise>
											</c:choose>
										</td>
										<td>
											<button onclick="del(this, '${entity.id}')" class="button red radius-3"><span class="icon-trash-o"></span> 删除</button>
											<button onclick="edit('${entity.id}')" class="button indigo radius-3"><span class="icon-edit-2"></span> 编辑</button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<!-- 分页 -->
						<div class="page">
							<ul id="page" class="pagination"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	var currentPage = "${pageInfo.pageNum}";
	var pageCount = "${pageInfo.pages}";
	javaex.page({
		id : "page",
		pageCount : pageCount,	// 总页数
		currentPage : currentPage,// 默认选中第几页
		// 返回当前选中的页数
		callback:function(rtn) {
			search(rtn.pageNum);
		}
	});
	
	/**
	 * 搜索
	 */
	function search(pageNum) {
		if (!pageNum) {
			pageNum = 1;
		}
		
		var keyword = $("#keyword").val();
		var status = "";
		if ($(':checkbox[name="status"]').is(":checked")) {
			status = "2";
		}
		
		window.location.href = "list.action"
				+ "?keyword="+keyword
				+ "&status="+status
				+ "&pageNum="+pageNum
				;
	}
	
	/**
	 * 添加/编辑用户
	 */
	function edit(id) {
		var title = "编辑用户";
		if (!id) {
			id = "";
			title = "添加用户";
		}
		javaex.dialog({
			type : "window",
			title : title,
			url : "edit.action?id="+id,
			width : "800",
			height : "550",
			isBackground : true
		});
	}
	
	/**
	 * 删除用户
	 */
	function del(obj, id) {
		javaex.deleteDialog(
			obj,	// obj是必须的
			{
				content : "确定要删除么？",
				callback : "deleteCallback('"+id+"')"
			}
		);
	}
	function deleteCallback(id) {
		javaex.optTip({
			content : "删除中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "delete.json",
			type : "POST",
			dataType : "json",
			data : {
				"id" : id
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	/**
	 * 跳转导入页面
	 */
	function importExcel() {
		javaex.dialog({
			type : "window",
			title : "导入",
			url : "${domain}/hgo_poi_demo/importExcel.action",
			width : "800",
			height : "550",
			isBackground : true
		});
	}
</script>
</html>
