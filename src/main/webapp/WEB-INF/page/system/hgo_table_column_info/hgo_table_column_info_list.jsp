<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>数据库字段管理 - ${system_name}</title>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<div class="content-header">
				<div class="breadcrumb">
					<span>系统设置</span>
					<span class="divider">/</span>
					<span class="active">表字段配置</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div class="list-content">
				<div class="block">
					<h2>${hgoTableInfo.tableName}表字段列表</h2>
					<!--右上角的返回按钮-->
					<button class="button indigo radius-3" onclick="history.back()" style="position: absolute;right: 20px;top: 16px;"><span class="icon-arrow_back"></span> 返回</button>
					
					<div class="main">
						<div class="operation-wrap">
							<div class="buttons-wrap">
								<button class="button blue radius-3" onclick="edit()"><span class="icon-plus"></span> 添加字段</button>
								<button class="button green radius-3" onclick="saveSort()"><span class="icon-check2"></span> 更新排序</button>
							</div>
						</div>
						<table id="table" class="table">
							<thead>
								<tr>
									<th class="checkbox"></th>
									<th>字段名</th>
									<th>表述</th>
									<th>字段类型</th>
									<th>字段长度</th>
									<th>默认值</th>
									<th>创建时间</th>
									<th>最后修改时间</th>
									<th>备注</th>
									<th>状态</th>
									<th style="width:60px;">排序</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list}" var="entity" varStatus="status" >
									<tr>
										<td>${status.count}</td>
										<td>${entity.columnName}</td>
										<td>${entity.chsname}</td>
										<td>${typeMap[entity.type]}</td>
										<td>${entity.length}</td>
										<td>${entity.defaultValue}</td>
										<td><fmt:formatDate value="${entity.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${entity.updateTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>${entity.remark}</td>
										<td>
											<c:choose>
												<c:when test="${entity.status=='1'}">
													<font color="green">启用</font>
												</c:when>
												<c:otherwise>
													<font color="red">废弃</font>
												</c:otherwise>
											</c:choose>
										</td>
										<td>
											<input type="hidden" name="id" value="${entity.id}" />
											<input type="text" class="text" name="sort" data-type="正整数" error-msg="必须输入正整数" value="${entity.sort}" autocomplete="off"/>
										</td>
										<td>
											<button onclick="del(this, '${entity.id}')" class="button red radius-3"><span class="icon-trash-o"></span> 删除</button>
											<button onclick="edit('${entity.id}')" class="button indigo radius-3"><span class="icon-edit-2"></span> 编辑</button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	var tableId = "${tableId}";
	/**
	 * 添加/编辑字段
	 */
	function edit(id) {
		var title = "编辑字段";
		if (!id) {
			id = "";
			title = "添加字段";
		}
		javaex.dialog({
			type : "window",
			title : title,
			url : "edit.action?tableId="+tableId+"&id="+id,
			width : "1000",
			height : "780",
			isBackground : true
		});
	}
	
	/**
	 * 删除字段
	 */
	function del(obj, id) {
		javaex.deleteDialog(
			obj,	// obj是必须的
			{
				content : "确定要删除么？",
				callback : "deleteCallback('"+id+"')"
			}
		);
	}
	function deleteCallback(id) {
		javaex.optTip({
			content : "删除中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "delete.json",
			type : "POST",
			dataType : "json",
			data : {
				"id" : id
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	/**
	 * 更新排序
	 */
	var idArr = new Array();
	var sortArr = new Array();
	function saveSort() {
		if (javaexVerify()) {
			idArr = [];
			sortArr = [];
			
			// id
			$('input[name="id"]').each(function() {
				idArr.push($(this).val());
			});
			
			// sort
			$('input[name="sort"]').each(function() {
				sortArr.push($(this).val());
			});
			
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "saveSort.json",
				type : "POST",
				dataType : "json",
				traditional : "true",
				data : {
					"idArr" : idArr,
					"sortArr" : sortArr
				},
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message
						});
						// 建议延迟加载
						setTimeout(function() {
							// 刷新页面
							window.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
</script>
</html>
