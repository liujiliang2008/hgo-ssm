<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>编辑表字段 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main" style="margin-top:10px;">
		<form id="form">
			<!--主键-->
			<input type="hidden" name="id" value="${hgoTableColumnInfo.id}"/>
			<!--表id-->
			<input type="hidden" name="tableId" value="${tableId}"/>
			
			<!--字段名-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">字段名</p></div>
				<div class="right">
					<input type="text" class="text" name="columnName" value="${hgoTableColumnInfo.columnName}" data-type="必填" autocomplete="off" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--表述-->
			<div class="unit">
				<div class="left"><p class="subtitle">表述</p></div>
				<div class="right">
					<input type="text" class="text" name="chsname" value="${hgoTableColumnInfo.chsname}" autocomplete="off" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--字段类型-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">字段类型</p></div>
				<div class="right">
					<select id="type" name="type" data-type="必填">
						<option value="">请选择</option>
						<c:forEach items="${typeList}" var="entity">
							<option value="${entity.value}" <c:if test="${entity.value==hgoTableColumnInfo.type}">selected</c:if>>${entity.name}</option>
						</c:forEach>
					</select>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--长度-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">长度</p></div>
				<div class="right">
					<input type="text" class="text" id="length" name="length" value="${hgoTableColumnInfo.length}" data-type="非负整数" error-msg="请填写正整数或0" autocomplete="off" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--约束-->
			<div class="unit">
				<div class="left"><p class="subtitle">约束</p></div>
				<div class="right">
					<input type="checkbox" class="fill" name="isPrimaryKey" value="1" <c:if test="${hgoTableColumnInfo.isPrimaryKey=='1'}">checked</c:if>/>设为主键
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--允许空-->
			<div class="unit">
				<div class="left"><p class="subtitle">不允许空</p></div>
				<div class="right">
					<input type="checkbox" class="fill" name="isNotNull" value="1" <c:if test="${hgoTableColumnInfo.isNotNull=='1'}">checked</c:if>/>不是null
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--小数点-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">小数点</p></div>
				<div class="right">
					<input type="text" class="text" id="point" name="point" value="${empty hgoTableColumnInfo.point?0:hgoTableColumnInfo.point}" data-type="非负整数" error-msg="请填写正整数或0" autocomplete="off" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--默认值-->
			<div class="unit">
				<div class="left"><p class="subtitle">默认值</p></div>
				<div class="right">
					<input type="text" class="text" id="defaultValue" name="defaultValue" value="${hgoTableColumnInfo.defaultValue}" autocomplete="off" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--备注-->
			<div class="unit">
				<div class="left"><p class="subtitle">备注</p></div>
				<div class="right">
					<textarea wrap="hard" class="desc" id="remark" name="remark">${hgoTableColumnInfo.remark}</textarea>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--状态-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">状态</p></div>
				<div class="right">
					<ul class="equal-6">
						<c:choose>
							<c:when test="${hgoTableColumnInfo.status=='0'}">
								<li><input type="radio" class="fill" name="status" value="1" />启用</li>
								<li><input type="radio" class="fill" name="status" value="0" checked/>废弃</li>
							</c:when>
							<c:otherwise>
								<li><input type="radio" class="fill" name="status" value="1" checked/>启用</li>
								<li><input type="radio" class="fill" name="status" value="0" />废弃</li>
							</c:otherwise>
						</c:choose>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--排序-->
			<div class="unit hide">
				<div class="left"><p class="subtitle">排序</p></div>
				<div class="right">
					<input type="text" class="text" name="sort" value="${empty hgoTableColumnInfo.sort?maxSort:hgoTableColumnInfo.sort}" data-type="正整数" error-msg="请填写正整数" autocomplete="off" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--提交按钮-->
			<div class="unit" style="float: right;">
				<div style="text-align: center;">
					<input type="button" onclick="closeDialog()" class="button no" style="margin: 0;" value="关闭" />
					<input type="button" onclick="save()" class="button yes" style="margin: 0;" value="保存" />
				</div>
			</div>
		</form>
	</div>
</body>
<script>
	javaex.select({
		id : "type",
		isInit : false,
		callback: function (rtn) {
			var selectValue = rtn.selectValue;
			if (selectValue=="int") {
				$("#length").val("11");
			} else if (selectValue=="smallint") {
				$("#length").val("4");
			} else if (selectValue=="mediumint") {
				$("#length").val("8");
			} else if (selectValue=="double") {
				$("#length").val("0");
			} else if (selectValue=="varchar") {
				$("#length").val("255");
			} else if (selectValue=="date" || selectValue=="datetime") {
				$("#length").val("0");
			} else if (selectValue=="tinyint") {
				$("#length").val("1");
			} else if (selectValue=="text") {
				$("#length").val("0");
			} else if (selectValue=="decimal") {
				$("#length").val("7");
			}
			// 是否禁用小数点
			if (selectValue=="decimal") {
				$("#point").val("2");
				$("#point").attr("disabled", false);
			} else {
				$("#point").val("0");
				$("#point").attr("disabled", true);
			}
		}
	});
	
	$(function() {
		// 格式化备注
		var remark = $("#remark").text();
		remark = remark.replace(/<br\/>/g, "\r\n");
		$("#remark").text(remark);
		
		setStatusByIsPrimaryKey();
	});
	$(':checkbox[name="isPrimaryKey"]').change(function() {
		setStatusByIsPrimaryKey();
	});
	function setStatusByIsPrimaryKey() {
		if ($(':checkbox[name="isPrimaryKey"]').is(":checked")) {
			$(':checkbox[name="isNotNull"]').attr("checked", true);
			$(':checkbox[name="isNotNull"]').attr("disabled", true);
			
			$("#defaultValue").attr("readonly", true);
			$("#defaultValue").addClass("readonly");
		} else {
			$(':checkbox[name="isNotNull"]').attr("disabled", false);
			
			$("#defaultValue").attr("readonly", false);
			$("#defaultValue").removeClass("readonly");
		}
	}
	
	/**
	 * 保存
	 */
	function save() {
		if (javaexVerify()) {
			$(':checkbox[name="isNotNull"]').attr("disabled", false);
			
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "save.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							// 关闭弹出层
							parent.javaex.close();
							// 刷新页面
							parent.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 关闭弹窗
	 */
	function closeDialog() {
		parent.javaex.close();
	}
</script>
</html>