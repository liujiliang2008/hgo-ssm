<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>数据库表 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main-10">
		<div class="operation-wrap">
			<div class="buttons-wrap">
				<button class="button blue radius-3" onclick="synchronousFromDatabase()"><span class="icon-plus"></span> 导出</button>
				
				<div class="input-group" style="float: right;">
					<input type="text" class="text" id="keyword" value="${keyword}" placeholder="表名" autocomplete="off"/>
					<button class="button blue" onclick="search()">搜索</button>
				</div>
			</div>
		</div>
		<table id="table" class="table">
			<thead>
				<tr>
					<th class="checkbox"><input type="checkbox" class="fill listen-1" /> </th>
					<th>序号</th>
					<th>表名</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${fn:length(pageInfo.list)==0}">
						<tr>
							<td colspan="3" style="text-align:center;">暂无记录</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${pageInfo.list}" var="TABLE_NAME" varStatus="status" >
							<tr>
								<td><input type="checkbox" class="fill listen-1-2" value="${TABLE_NAME}" name="tableName" /> </td>
								<td>${status.count}</td>
								<td>${TABLE_NAME}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<!-- 分页 -->
		<div class="page">
			<ul id="page" class="pagination"></ul>
		</div>
	</div>
</body>
<script>
	var currentPage = "${pageInfo.pageNum}";
	var pageCount = "${pageInfo.pages}";
	javaex.page({
		id : "page",
		pageCount : pageCount,	// 总页数
		currentPage : currentPage,// 默认选中第几页
		// 返回当前选中的页数
		callback:function(rtn) {
			search(rtn.pageNum);
		}
	});
	
	/**
	 * 搜索
	 */
	function search(pageNum) {
		if (!pageNum) {
			pageNum = 1;
		}
		
		var keyword = $("#keyword").val();
		window.location.href = "listTableFromDatabase.action"
				+ "?keyword="+keyword
				+ "&pageNum="+pageNum
				;
	}
	
	/**
	 * 从数据库导出
	 */
	function synchronousFromDatabase() {
		var tableNameArr = new Array();
		$(':checkbox[name="tableName"]:checked').each(function() {
			tableNameArr.push($(this).val());
		});
		// 判断至少选择一条记录
		if (tableNameArr.length==0) {
			javaex.optTip({
				content : "至少选择一条记录",
				type : "error"
			});
			return;
		}
		
		javaex.optTip({
			content : "数据同步中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "synchronousFromDatabase.json",
			type : "POST",
			dataType : "json",
			traditional : "true",
			data : {
				"tableNameArr" : tableNameArr
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
</script>
</html>
