<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>编辑表 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main" style="margin-top:10px;">
		<form id="form">
			<input type="hidden" name="id" value="${hgoTableInfo.id}"/>
			<!--表名-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">表名</p></div>
				<div class="right">
					<input type="text" class="text" name="tableName" value="${hgoTableInfo.tableName}" data-type="必填" autocomplete="off" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--表述-->
			<div class="unit">
				<div class="left"><p class="subtitle">表述</p></div>
				<div class="right">
					<input type="text" class="text" name="chsname" value="${hgoTableInfo.chsname}" autocomplete="off" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--备注-->
			<div class="unit">
				<div class="left"><p class="subtitle">备注</p></div>
				<div class="right">
					<textarea wrap="hard" class="desc" id="remark" name="remark">${hgoTableInfo.remark}</textarea>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--状态-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">状态</p></div>
				<div class="right">
					<ul class="equal-6">
						<c:choose>
							<c:when test="${hgoTableInfo.status=='0'}">
								<li><input type="radio" class="fill" name="status" value="1" />启用</li>
								<li><input type="radio" class="fill" name="status" value="0" checked/>废弃</li>
							</c:when>
							<c:otherwise>
								<li><input type="radio" class="fill" name="status" value="1" checked/>启用</li>
								<li><input type="radio" class="fill" name="status" value="0" />废弃</li>
							</c:otherwise>
						</c:choose>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--提交按钮-->
			<div class="unit iframe-button">
				<div style="text-align: center;">
					<input type="button" onclick="closeDialog()" class="button no" value="关闭" />
					<input type="button" onclick="save()" class="button yes" value="保存" />
				</div>
			</div>
		</form>
	</div>
</body>
<script>
	/**
	 * 保存
	 */
	function save() {
		if (javaexVerify()) {
			var remark = $("#remark").text();
			remark = remark.replace(/<(script)[\S\s]*?\1>|<\/?(a|img)[^>]*>/gi, "").replace(/\r\n/g, "<br/>").replace(/\n/g,"<br/>");
			$("#remark").text(remark);
			
			javaex.optTip({
				content : "数据提交中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "save.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							// 关闭弹出层
							parent.javaex.close();
							// 刷新页面
							parent.location.reload();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 关闭弹窗
	 */
	function closeDialog() {
		parent.javaex.close();
	}
</script>
</html>