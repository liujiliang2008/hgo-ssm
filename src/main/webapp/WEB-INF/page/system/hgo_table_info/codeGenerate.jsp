<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>代码生成 - ${system_name}</title>
<style>
body {
	background-color:#fff;
}
</style>
</head>

<body>
	<div class="main" style="margin-top:-10px;">
		<p class="tip">您选择的表是：${tip}</p>
		<form id="form">
			<input type="hidden" name="idArrStr" value="${idArrStr}"/>
			<!--项目路径-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">项目路径</p></div>
				<div class="right">
					<input type="text" class="text" name="projectPath" value="" data-type="必填" />
					<p class="hint">例如：G:\tool\workspace\hgo</p>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--包名-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">包名</p></div>
				<div class="right">
					<input type="text" class="text" name="packageName" value="" data-type="必填" />
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--主键策略-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">主键策略</p></div>
				<div class="right">
					<ul class="equal-6">
						<li><input type="radio" class="fill" name="primaryKeyType" value="AUTO_INCREMENT" checked/>自增长</li>
						<li><input type="radio" class="fill" name="primaryKeyType" value="UUID" />UUID</li>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--是否分页-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">是否分页</p></div>
				<div class="right">
					<ul class="equal-6">
						<li><input type="radio" class="fill" name="isPage" value="1" checked/>分页</li>
						<li><input type="radio" class="fill" name="isPage" value="0" />不分页</li>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--编辑页-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">编辑页</p></div>
				<div class="right">
					<ul class="equal-6">
						<li><input type="radio" class="fill" name="editType" value="popup" checked/>弹窗</li>
						<li><input type="radio" class="fill" name="editType" value="jumpPage" />跳转页面</li>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--生成代码-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">生成代码</p></div>
				<div class="right">
					<ul class="equal-5">
						<li><input type="checkbox" class="fill" name="codeType" value="controller" />controller</li>
						<li><input type="checkbox" class="fill" name="codeType" value="service" />service</li>
						<li><input type="checkbox" class="fill" name="codeType" value="dao" />dao</li>
						<li><input type="checkbox" class="fill" name="codeType" value="view" />view</li>
						<li><input type="checkbox" class="fill" name="codeType" value="jsp" />jsp</li>
						<span class="clearfix"></span>
					</ul>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--开发场景-->
			<div class="unit">
				<div class="left"><span class="required">*</span><p class="subtitle">开发场景</p></div>
				<div class="right">
					<ul class="equal-6">
						<li><input type="radio" class="fill" name="mode" value="business" checked/>业务开发</li>
						<li><input type="radio" class="fill" name="mode" value="plugin" />插件开发</li>
						<span class="clearfix"></span>
					</ul>
					<p class="hint">
						业务开发：代码会生成在action包下。并且controller、service、dao层会按照表名分包<br/>
						插件开发：代码会生成在plugin包下。并且controller、service、dao层不会按照表名分包，因为插件是独立的，不会需要很多表
					</p>
				</div>
				<span class="clearfix"></span>
			</div>
			
			<!--提交按钮-->
			<div class="unit iframe-button">
				<div style="text-align: center;">
					<input type="button" onclick="closeDialog()" class="button no" value="关闭" />
					<input type="button" onclick="save()" class="button yes" value="生成" />
				</div>
			</div>
		</form>
	</div>
</body>
<script>
	var idArrStr = "${idArrStr}";
	/**
	 * 保存
	 */
	function save() {
		if (javaexVerify()) {
			javaex.optTip({
				content : "代码生成中，请稍候...",
				type : "submit"
			});
			
			$.ajax({
				url : "codeGenerate.json",
				type : "POST",
				dataType : "json",
				data : $("#form").serialize(),
				success : function(rtn) {
					if (rtn.code=="000000") {
						javaex.optTip({
							content : rtn.message,
							type : "success"
						});
						// 建议延迟加载
						setTimeout(function() {
							// 关闭弹出层
							parent.javaex.close();
						}, 1000);
					} else {
						javaex.optTip({
							content : rtn.message,
							type : "error"
						});
					}
				}
			});
		}
	}
	
	/**
	 * 关闭弹窗
	 */
	function closeDialog() {
		parent.javaex.close();
	}
</script>
</html>