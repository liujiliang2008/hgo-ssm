<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>数据库表管理 - ${system_name}</title>
</head>

<body>
	<!-- 左侧菜单 -->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<div class="content-header">
				<div class="breadcrumb">
					<span>系统设置</span>
					<span class="divider">/</span>
					<span class="active">表单管理</span>
				</div>
			</div>
			
			<!--主体内容-->
			<div class="list-content">
				<div class="block">
					<h2>数据库表列表</h2>
					<div class="main">
						<div class="admin-search">
							<div class="input-group">
								<input type="text" class="text" id="keyword" value="${keyword}" placeholder="表名" autocomplete="off"/>
								<button class="button blue" onclick="search()">搜索</button>
							</div>
						</div>
						
						<div class="operation-wrap">
							<div class="buttons-wrap">
								<button class="button blue radius-3" onclick="edit()"><span class="icon-plus"></span> 添加</button>
								<button class="button yellow radius-3" onclick="synchronousToDatabase()"><span class="icon-layers"></span> 同步到数据库</button>
								<button class="button green radius-3" onclick="codeGenerate()"><span class="icon-document-alt-fill"></span> 代码生成</button>
								
								<button class="button yellow radius-3" style="float: right;" onclick="listTableFromDatabase()"><span class="icon-cloud_download"></span> 从数据库导出</button>
							</div>
						</div>
						<table id="table" class="table">
							<thead>
								<tr>
									<th class="checkbox"><input type="checkbox" class="fill listen-1" /> </th>
									<th style="width:40px;">序号</th>
									<th>表名</th>
									<th>表述</th>
									<th>创建时间</th>
									<th>最后修改时间</th>
									<th>备注</th>
									<th>状态</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${pageInfo.list}" var="entity" varStatus="status" >
									<tr>
										<td><input type="checkbox" class="fill listen-1-2" value="${entity.id}" name="id" /> </td>
										<td>${status.count}</td>
										<td>${entity.tableName}</td>
										<td>${entity.chsname}</td>
										<td><fmt:formatDate value="${entity.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${entity.updateTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>${entity.remark}</td>
										<td>
											<c:choose>
												<c:when test="${entity.status=='1'}">
													<font color="green">启用</font>
												</c:when>
												<c:otherwise>
													<font color="red">废弃</font>
												</c:otherwise>
											</c:choose>
										</td>
										<td>
											<button class="button red radius-3" onclick="del(this, '${entity.id}')"><span class="icon-trash-o"></span> 删除</button>
											<button class="button indigo radius-3" onclick="edit('${entity.id}')"><span class="icon-edit-2"></span> 编辑</button>
											<a class="button green radius-3" href="${domain}/hgo_table_column_info/list.action?tableId=${entity.id}"><span class="icon-grid"></span> 字段管理</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<!-- 分页 -->
						<div class="page">
							<ul id="page" class="pagination"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	var currentPage = "${pageInfo.pageNum}";
	var pageCount = "${pageInfo.pages}";
	javaex.page({
		id : "page",
		pageCount : pageCount,	// 总页数
		currentPage : currentPage,// 默认选中第几页
		// 返回当前选中的页数
		callback:function(rtn) {
			search(rtn.pageNum);
		}
	});
	
	/**
	 * 搜索
	 */
	function search(pageNum) {
		if (!pageNum) {
			pageNum = 1;
		}
		
		var keyword = $("#keyword").val();
		var status = "";
		if ($(':checkbox[name="status"]').is(":checked")) {
			status = "2";
		}
		
		window.location.href = "list.action"
				+ "?keyword="+keyword
				+ "&status="+status
				+ "&pageNum="+pageNum
				;
	}
	
	/**
	 * 添加/编辑表
	 */
	function edit(id) {
		var title = "编辑表";
		if (!id) {
			id = "";
			title = "添加表";
		}
		javaex.dialog({
			type : "window",
			title : title,
			url : "edit.action?id="+id,
			width : "800",
			height : "550",
			isBackground : true
		});
	}
	
	/**
	 * 删除表
	 */
	function del(obj, id) {
		javaex.deleteDialog(
			obj,	// obj是必须的
			{
				content : "确定要删除么？不会影响数据库",
				callback : "deleteCallback('"+id+"')"
			}
		);
	}
	function deleteCallback(id) {
		javaex.optTip({
			content : "删除中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "delete.json",
			type : "POST",
			dataType : "json",
			data : {
				"id" : id
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
					
					// 建议延迟加载
					setTimeout(function() {
						// 刷新页面
						window.location.reload();
					}, 1000);
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	/**
	 * 同步到数据库
	 */
	var idArr = new Array();
	function synchronousToDatabase() {
		idArr = [];
		$(':checkbox[name="id"]:checked').each(function() {
			idArr.push($(this).val());
		});
		// 判断至少选择一条记录
		if (idArr.length==0) {
			javaex.optTip({
				content : "至少选择一条记录",
				type : "error"
			});
			return;
		}
		
		var html = '<input type="radio" class="fill" name="synchronousType" value="1" checked />普通同步<br/>';
		html += '<input type="radio" class="fill" name="synchronousType"  value="2" />强制同步（先删除已存在的表，再创建）';
		javaex.confirm({
			content : html,
			width : "400px",
			callback : "synchronousCallback()"
		});
		javaex.render();
	}
	function synchronousCallback() {
		var synchronousType = $(':radio[name="synchronousType"]:checked').val();
		javaex.optTip({
			content : "数据同步中，请稍候...",
			type : "submit"
		});
		
		$.ajax({
			url : "synchronousToDatabase.json",
			type : "POST",
			dataType : "json",
			traditional : "true",
			data : {
				"idArr" : idArr,
				"synchronousType" : synchronousType
			},
			success : function(rtn) {
				if (rtn.code=="000000") {
					javaex.optTip({
						content : rtn.message,
						type : "success"
					});
				} else {
					javaex.optTip({
						content : rtn.message,
						type : "error"
					});
				}
			}
		});
	}
	
	/**
	 * 从数据库导出
	 */
	function listTableFromDatabase() {
		javaex.dialog({
			type : "window",
			title : "从数据库导出表",
			url : "listTableFromDatabase.action",
			width : "1000",
			height : "650",
			isBackground : true,
			isClickMaskClose : true,
			closeX : "callback()"
		});
	}
	function callback() {
		window.location.reload();
	}
	
	/**
	 * 代码生成
	 */
	function codeGenerate() {
		var idArrStr = "";
		$(':checkbox[name="id"]:checked').each(function() {
			if (idArrStr=="") {
				idArrStr = $(this).val();
			} else {
				idArrStr += "_" + $(this).val();
			}
		});
		// 判断至少选择一条记录
		if (idArrStr=="") {
			javaex.optTip({
				content : "至少选择一条记录",
				type : "error"
			});
			return;
		}
		
		javaex.dialog({
			type : "window",
			title : "代码生成",
			url : "codeGenerate.action?idArrStr="+idArrStr,
			width : "950",
			height : "680",
			isBackground : true
		});
	}
</script>
</html>
