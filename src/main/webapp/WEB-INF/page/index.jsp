<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<%@ include file="/style.jsp"%>
<title>首页 - ${system_name}</title>
</head>

<body>
	<!--左侧内容-->
	<c:import url="/WEB-INF/page/common/menu.jsp"></c:import>
	
	<!--右侧内容-->
	<div class="admin-right">
		<!--顶部-->
		<c:import url="/WEB-INF/page/common/header.jsp"></c:import>
		
		<!--内容-->
		<div class="admin-content">
			<!--面包屑导航-->
			<div class="content-header">
				<div class="breadcrumb">
					<span>主页</span>
				</div>
			</div>
			
			<!--全部主体内容-->
			<div class="list-content">
				<!--块元素-->
				<div class="block">
					<!--修饰块元素名称-->
					<div class="banner">
						<p class="tab fixed">Hgo敏捷开发脚手架</p>
					</div>
					
					<!--正文内容-->
					<div class="main">
						<div class='unit'>
							<div class='left'><p class='subtitle'>程序版本：</p></div>
							<div class='right'>hgo 1.0</div>
							<span class='clearfix'></span>
						</div>
						<div class='unit'>
							<div class='left'><p class='subtitle'>QQ群：</p></div>
							<div class='right'>587243028</div>
							<span class='clearfix'></span>
						</div>
						<div class='unit'>
							<div class='left'><p class='subtitle'>作者：</p></div>
							<div class='right'>陈霓清</div>
							<span class='clearfix'></span>
						</div>
						<div class='unit'>
							<div class='left'><p class='subtitle'>前端：</p></div>
							<div class='right'>javaex前端框架</div>
							<span class='clearfix'></span>
						</div>
						<div class='unit'>
							<div class='left'><p class='subtitle'>后端：</p></div>
							<div class='right'>SSM</div>
							<span class='clearfix'></span>
						</div>
						<div class='unit'>
							<div class='left'><p class='subtitle'>安全框架：</p></div>
							<div class='right'>shiro</div>
							<span class='clearfix'></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>