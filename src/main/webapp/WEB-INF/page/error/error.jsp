<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>/(ㄒoㄒ)/~~ - ${system_name}</title>
<style>
* {
	padding:0;
	margin:0;
}
body {
	background-color: #ffffff;
}
a {
	text-decoration:none;
}
.error-p {
	line-height: 24px;
	font-size: 24px;
	padding-bottom: 15px;
	border-bottom: 1px solid #f6f6f6;
	text-align: center;
	color: #262b31;
	margin-top: 100px;
}
.error-btn-container {
	margin: 40px auto 0;
	text-align: center;
}
.error-btn-container .error-btn {
	display: inline-block;
	border: 1.5px solid #000000;
	background-color: #ef4836;
	color: #000000;
	font-size: 24px;
	border-radius: 50px;
	text-align: center;
	padding: 10px 60px;
	line-height: 24px;
	white-space: nowrap;
	font-weight: 500;
}
</style>
</head>

<body>
	<p class="error-p">${message}</p>
	<div class="error-btn-container">
		<a class="error-btn" href="javascript:;" onclick="goback()">返回</a>
	</div>
</body>
<script>
	function goback() {
		if (self!=top) {
			parent.javaex.close();
		} else {
			history.back();
		}
	}
</script>
</html>
