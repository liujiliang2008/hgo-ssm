<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="shortcut icon" href=${domain}/static/images/favicon.ico type=image/x-icon>
<!--字体图标-->
<link href="${domain}/static/plugin/javaex/pc/css/icomoon.css" rel="stylesheet" />
<!--动画-->
<link href="${domain}/static/plugin/javaex/pc/css/animate.css" rel="stylesheet" />
<!--骨架样式-->
<link href="${domain}/static/plugin/javaex/pc/css/common.css" rel="stylesheet" />
<!--皮肤（缇娜）-->
<link href="${domain}/static/plugin/javaex/pc/css/skin/tina.css" rel="stylesheet" />
<!--jquery，请勿轻易修改版本-->
<script src="${domain}/static/plugin/javaex/pc/lib/jquery-1.7.2.min.js"></script>
<!--全局动态修改-->
<script src="${domain}/static/plugin/javaex/pc/js/common.js"></script>
<!--核心组件-->
<script src="${domain}/static/plugin/javaex/pc/js/javaex.min.js"></script>
<!--表单验证-->
<script src="${domain}/static/plugin/javaex/pc/js/javaex-formVerify.js"></script>
