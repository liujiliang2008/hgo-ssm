package cn.javaex.hgo.action.system.dao.hgo_user_role;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface IHgoUserRoleDAO {

	/**
	 * 根据用户id，查询用户角色
	 * @param userId
	 * @return
	 */
	List<Integer> listByUserId(String userId);
	
	/**
	 * 向角色添加用户
	 * @param roleId
	 * @param userIdArr
	 */
	int insertUser(@Param("roleId") String roleId, @Param("userIdArr") String[] userIdArr);

	/**
	 * 移除用户
	 * @param roleId
	 * @param userId
	 */
	int deleteUser(@Param("roleId") String roleId, @Param("userId") String userId);

	/**
	 * 批量移除用户
	 * @param roleId
	 * @param userIdArr
	 * @return
	 */
	int batchDeleteUser(@Param("roleId") String roleId, @Param("userIdArr") String[] userIdArr);

}