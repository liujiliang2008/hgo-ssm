package cn.javaex.hgo.action.system.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.javaex.hgo.action.system.service.hgo_user_info.HgoUserInfoService;
import cn.javaex.hgo.config.constant.HgoErrorMsg;
import cn.javaex.hgo.config.exception.HgoException;
import cn.javaex.hgo.util.Result;

@Controller
public class Index {

	@Autowired
	private HgoUserInfoService hgoUserInfoService;
	
	/**
	 * 跳转登录页面
	 * @return
	 */
	@RequestMapping("login.action")
	public String login() {
		return "login";
	}
	
	/**
	 * 登录校验
	 * @param request
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("login.json")
	@ResponseBody
	public Result login(HttpServletRequest request) throws HgoException {
		// 1.0 获取参数
		String loginName = request.getParameter("loginName");
		String password = request.getParameter("password");
		
		// 2.0 校验参数
		if (StringUtils.isBlank(loginName) || StringUtils.isBlank(password)) {
			throw new HgoException(HgoErrorMsg.ERROR_100001);
		}
		
		// 3.0 执行登录判断
		Md5Hash md5 = new Md5Hash(password);
		String encryptPassword = md5.toString();
		hgoUserInfoService.login(loginName.trim(), encryptPassword);
		
		return Result.success();
	}
	
	/**
	 * 跳转首页
	 */
	@RequestMapping("index.action")
	public String index() {
		return "index";
	}
}