package cn.javaex.hgo.action.system.dao.hgo_role_menu;

import java.util.List;

import cn.javaex.hgo.action.system.view.HgoRoleMenu;

public interface IHgoRoleMenuDAO {

	/**
	 * 删除该角色的菜单
	 * @param roleId
	 */
	int deleteByRoleId(String roleId);

	/**
	 * 插入角色-菜单分配记录
	 * @param hgoRoleMenu
	 * @return
	 */
	int insert(HgoRoleMenu hgoRoleMenu);

	/**
	 * 根据角色id，获取该角色已经分配的菜单
	 * @param roleId
	 * @return
	 */
	List<Integer> listMenuIdByRoleId(String roleId);

	/**
	 * 根据指定的菜单id，查询被分配的角色id list
	 * @param menuId 菜单id
	 * @return
	 */
	List<Integer> listRoleIdByMenuId(String menuId);

}