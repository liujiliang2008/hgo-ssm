package cn.javaex.hgo.action.system.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 菜单表
 * 
 * @author 陈霓清
 */
public class HgoMenuInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;			// 主键
	private String name;		// 菜单名称
	private String parentId;	// 父节点
	private String url;			// 菜单链接
	private Integer sort;		// 排序
	private String icon;		// 图标
	private String permCode;	// 按钮权限标识
	private String type;		// 类型（目录、菜单、按钮）
	private Integer isSystem;	// 是否是系统内置的（1：是	0：用户创建）
	
	private boolean checked;	// 是否选中（权限配置时使用）
	private boolean open;		// 是否展开（权限配置时使用）
	private List<HgoMenuInfo> childrenList = new ArrayList<HgoMenuInfo>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getPermCode() {
		return permCode;
	}
	public void setPermCode(String permCode) {
		this.permCode = permCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getIsSystem() {
		return isSystem;
	}
	public void setIsSystem(Integer isSystem) {
		this.isSystem = isSystem;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public List<HgoMenuInfo> getChildrenList() {
		return childrenList;
	}
	public void setChildrenList(List<HgoMenuInfo> childrenList) {
		this.childrenList = childrenList;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
