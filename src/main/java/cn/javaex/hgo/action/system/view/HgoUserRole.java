package cn.javaex.hgo.action.system.view;

/**
 * 用户角色表（为用户配置多个角色，每个角色下有各自权限的菜单）
 * 
 * @author 陈霓清
 */
public class HgoUserRole {
	private String userId;	// 用户id
	private String roleId;	// 角色id
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
}
