package cn.javaex.hgo.action.system.controller.hgo_role_info;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.javaex.hgo.action.system.service.hgo_role_info.HgoRoleInfoService;
import cn.javaex.hgo.action.system.view.HgoRoleInfo;
import cn.javaex.hgo.config.exception.HgoException;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("hgo_role_info")
public class HgoRoleInfoController {

	@Autowired
	private HgoRoleInfoService hgoRoleInfoService;
	
	/**
	 * 查询所有角色
	 * @param map
	 * @return
	 */
	@RequestMapping("list.action")
	public String list(ModelMap map,
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize) {
		
		// pageHelper分页插件
		// 只需要在查询之前调用，传入当前页码，以及每一页显示多少条
		PageHelper.startPage(pageNum, pageSize);
		List<HgoRoleInfo> list = hgoRoleInfoService.list();
		PageInfo<HgoRoleInfo> pageInfo = new PageInfo<HgoRoleInfo>(list);
		map.put("pageInfo", pageInfo);
		
		return "system/hgo_role_info/hgo_role_info_list";
	}
	
	/**
	 * 跳转角色编辑页面
	 * @param map
	 * @param id 角色id
	 * @return
	 */
	@RequestMapping("edit.action")
	public String edit(ModelMap map,
			@RequestParam(required=false, value="id") String id) {
		
		// 查询角色
		if (StringUtils.isNotEmpty(id)) {
			HgoRoleInfo hgoRoleInfo = hgoRoleInfoService.selectById(id);
			if (hgoRoleInfo!=null) {
				map.put("hgoRoleInfo", hgoRoleInfo);
			}
		}
		
		// 查询目前最大排序
		int maxSort = hgoRoleInfoService.selectMaxSort();
		map.put("maxSort", maxSort+1);
		
		return "system/hgo_role_info/hgo_role_info_edit";
	}
	
	/**
	 * 保存角色
	 * @param hgoRoleInfo
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public Result save(HgoRoleInfo hgoRoleInfo) throws HgoException {
		
		hgoRoleInfoService.save(hgoRoleInfo);
		
		return Result.success();
	}
	
	/**
	 * 删除角色
	 * @param id 角色id
	 * @return
	 */
	@RequestMapping("delete.json")
	@ResponseBody
	public Result delete(@RequestParam(value="id") String id) {
		
		hgoRoleInfoService.deleteById(id);
		
		return Result.success();
	}
}
