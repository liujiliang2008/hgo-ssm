package cn.javaex.hgo.action.system.service.code_generate;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import cn.javaex.hgo.action.system.view.HgoTableColumnInfo;
import cn.javaex.hgo.action.system.view.HgoTableInfo;
import cn.javaex.hgo.config.constant.HgoConstant;
import cn.javaex.hgo.util.HgoStringUtils;

@Service("CodeGenerateService")
public class CodeGenerateService {
	
	private String changeType(String type) {
		if (type.equals("int") || type.equals("smallint") || type.equals("tinyint")) {
			return "Integer";
		}
		if (type.equals("double")) {
			return "double";
		}
		if (type.equals("varchar") || type.equals("text")) {
			return "String";
		}
		if (type.equals("date") || type.equals("datetime")) {
			return "Date";
		}
		if (type.equals("decimal")) {
			return "BigDecimal";
		}
		return "String";
	}
	
	public void generateController(Map<String, Object> param, HgoTableInfo hgoTableInfo,
			List<HgoTableColumnInfo> columnList) throws IOException {
		String domain = param.get("domain").toString();
		String projectPath = param.get("projectPath").toString();
		File generatorDir = new File(projectPath);
		String mode = param.get("mode").toString();
		File javaDir = new File(generatorDir, "src/main/java");
		javaDir.mkdirs();
		String packageName = param.get("packageName").toString();
		String projectCode = HgoStringUtils.toUnderlineName(packageName);
		String basePackage = "cn.javaex.hgo.action." + projectCode + ".";
		if ("plugin".equals(mode)) {
			basePackage = "cn.javaex.hgo.plugin." + projectCode + ".";
		}
		Map<String, String> replaceMap = new HashMap<String, String>();
		if ("business".equals(mode)) {
			replaceMap.put("#jspPath#", packageName.replace(".", "/") + "/" + hgoTableInfo.getTableName());
		} else {
			replaceMap.put("#jspPath#", "plugin/" + packageName.replace(".", "/"));
		}
		String entityPackage = basePackage + "view";
		replaceMap.put("#EntityPackage#", entityPackage);
		String entityClassName = HgoStringUtils.toCapitalizeCamelCase(hgoTableInfo.getTableName());
		replaceMap.put("#EntityClassName#", entityClassName);
		String entityObjectName = HgoStringUtils.initLower(entityClassName);
		replaceMap.put("#EntityObjectName#", entityObjectName);
		String servicePackagePath = basePackage + "service." + hgoTableInfo.getTableName();
		if ("plugin".equals(mode)) {
			servicePackagePath = basePackage + "service";
		}
		replaceMap.put("#ServicePackage#", servicePackagePath);
		String controllerPackagePath = basePackage + "controller." + hgoTableInfo.getTableName();
		if ("plugin".equals(mode)) {
			controllerPackagePath = basePackage + "controller";
		}
		replaceMap.put("#ControllerPackage#", controllerPackagePath);
		String serviceClassName = entityClassName + "Service";
		replaceMap.put("#ServiceClassName#", serviceClassName);
		replaceMap.put("#ServiceObjectName#", HgoStringUtils.initLower(serviceClassName));
		String controllerClassName = entityClassName + "Controller";
		replaceMap.put("#ControllerClassName#", controllerClassName);
		replaceMap.put("#TableName#", hgoTableInfo.getTableName());
		for (HgoTableColumnInfo hgoTableColumnInfo : columnList) {
			if (hgoTableColumnInfo.getIsPrimaryKey()==1) {
				replaceMap.put("#ColumnIdPropertyName#", HgoStringUtils.toCamelCase(hgoTableColumnInfo.getColumnName()));
				break;
			}
		}
		String importEntityList = "import " + entityPackage + ".*;" + System.lineSeparator();
		replaceMap.put("#ImportEntityList#", importEntityList);
		String templateUrl = domain + HgoConstant.GENERATE_TEMPLATE;
		File templateFile = null;
		String isPage = param.get("isPage").toString();
		if ("1".equals(isPage)) {
			templateFile = new File(templateUrl, "ControllerTemplate.java");
		} else {
			templateFile = new File(templateUrl, "ControllerTemplate_noPage.java");
		}
		String content = FileUtils.readFileToString(templateFile, "UTF-8");
		for (String key : replaceMap.keySet()) {
			String value = replaceMap.get(key);
			content = content.replaceAll(key, value);
		}
		File fileDir = new File(javaDir, controllerPackagePath.replaceAll("\\.", "\\"+File.separator));
		fileDir.mkdirs();
		File classFile = new File(fileDir, controllerClassName+".java");
		FileUtils.writeStringToFile(classFile, content, "UTF-8");
	}

	public void generateService(Map<String, Object> param, HgoTableInfo hgoTableInfo,
			List<HgoTableColumnInfo> columnList) throws IOException {
		String domain = param.get("domain").toString();
		String projectPath = param.get("projectPath").toString();
		File generatorDir = new File(projectPath);
		String mode = param.get("mode").toString();
		File javaDir = new File(generatorDir, "src/main/java");
		javaDir.mkdirs();
		String packageName = param.get("packageName").toString();
		String projectCode = HgoStringUtils.toUnderlineName(packageName);
		String basePackage = "cn.javaex.hgo.action." + projectCode + ".";
		if ("plugin".equals(mode)) {
			basePackage = "cn.javaex.hgo.plugin." + projectCode + ".";
		}
		Map<String, String> replaceMap = new HashMap<String, String>();
		String entityPackage = basePackage + "view";
		replaceMap.put("#EntityPackage#", entityPackage);
		String entityClassName = HgoStringUtils.toCapitalizeCamelCase(hgoTableInfo.getTableName());
		replaceMap.put("#EntityClassName#", entityClassName);
		String entityObjectName = HgoStringUtils.initLower(entityClassName);
		replaceMap.put("#EntityObjectName#", entityObjectName);
		String daoPackagePath = basePackage + "dao." + hgoTableInfo.getTableName();
		if ("plugin".equals(mode)) {
			daoPackagePath = basePackage + "dao";
		}
		replaceMap.put("#DAOPackage#", daoPackagePath);
		String servicePackagePath = basePackage + "service." + hgoTableInfo.getTableName();
		if ("plugin".equals(mode)) {
			servicePackagePath = basePackage + "service";
		}
		replaceMap.put("#ServicePackage#", servicePackagePath);
		String daoClassName = "I" + entityClassName + "DAO";
		replaceMap.put("#DAOClassName#", daoClassName);
		replaceMap.put("#DAOObjectName#", HgoStringUtils.initLower(daoClassName));
		String serviceClassName = entityClassName + "Service";
		replaceMap.put("#ServiceClassName#", serviceClassName);
		for (HgoTableColumnInfo hgoTableColumnInfo : columnList) {
			if (hgoTableColumnInfo.getIsPrimaryKey()==1) {
				String columnIdCapName = HgoStringUtils.toCapitalizeCamelCase(hgoTableColumnInfo.getColumnName());
				replaceMap.put("#ColumnIdCapName#", columnIdCapName);
				replaceMap.put("#ColumnIdPropertyName#", HgoStringUtils.toCamelCase(hgoTableColumnInfo.getColumnName()));
				String primaryKeyType = param.get("primaryKeyType").toString();
				if ("UUID".equals(primaryKeyType)) {
					replaceMap.put("#importUUID#", "import java.util.UUID;"+System.lineSeparator());
					replaceMap.put("#UUID#", System.lineSeparator()+"			"+entityObjectName+".set"+columnIdCapName+"(UUID.randomUUID().toString().replace(\"-\", \"\"));");
				} else {
					replaceMap.put("#importUUID#", "");
					replaceMap.put("#UUID#", "");
				}
				break;
			}
		}
		String importEntityList = "import " + entityPackage + ".*;" + System.lineSeparator();
		replaceMap.put("#ImportEntityList#", importEntityList);
		String templateUrl = domain + HgoConstant.GENERATE_TEMPLATE;
		File templateFile = new File(templateUrl, "ServiceTemplate.java");
		String content = FileUtils.readFileToString(templateFile, "UTF-8");
		for (String key : replaceMap.keySet()) {
			String value = replaceMap.get(key);
			content = content.replaceAll(key, value);
		}
		File fileDir = new File(javaDir, servicePackagePath.replaceAll("\\.", "\\"+File.separator));
		fileDir.mkdirs();
		File classFile = new File(fileDir, serviceClassName+".java");
		FileUtils.writeStringToFile(classFile, content, "UTF-8");
	}
	
	public void generateDaoJava(Map<String, Object> param, HgoTableInfo hgoTableInfo,
			List<HgoTableColumnInfo> columnList) throws IOException {
		String domain = param.get("domain").toString();
		String projectPath = param.get("projectPath").toString();
		File generatorDir = new File(projectPath);
		String mode = param.get("mode").toString();
		File javaDir = new File(generatorDir, "src/main/java");
		javaDir.mkdirs();
		String packageName = param.get("packageName").toString();
		String projectCode = HgoStringUtils.toUnderlineName(packageName);
		String basePackage = "cn.javaex.hgo.action." + projectCode + ".";
		if ("plugin".equals(mode)) {
			basePackage = "cn.javaex.hgo.plugin." + projectCode + ".";
		}
		Map<String, String> replaceMap = new HashMap<String, String>();
		String daoPackagePath = basePackage + "dao." + hgoTableInfo.getTableName();
		if ("plugin".equals(mode)) {
			daoPackagePath = basePackage + "dao";
		}
		replaceMap.put("#DAOPackage#", daoPackagePath);
		String entityPackage = basePackage + "view";
		replaceMap.put("#EntityPackage#", entityPackage);
		String entityClassName = HgoStringUtils.toCapitalizeCamelCase(hgoTableInfo.getTableName());
		replaceMap.put("#EntityClassName#", entityClassName);
		String entityObjectName = HgoStringUtils.initLower(entityClassName);
		replaceMap.put("#EntityObjectName#", entityObjectName);
		String daoClassName = "I" + entityClassName + "DAO";
		replaceMap.put("#DAOClassName#", daoClassName);
		for (HgoTableColumnInfo hgoTableColumnInfo : columnList) {
			if (hgoTableColumnInfo.getIsPrimaryKey()==1) {
				replaceMap.put("#ColumnIdPropertyName#", HgoStringUtils.toCamelCase(hgoTableColumnInfo.getColumnName()));
				break;
			}
		}
		String templateUrl = domain + HgoConstant.GENERATE_TEMPLATE;
		File templateFile = new File(templateUrl, "DAOTemplate.java");
		String content = FileUtils.readFileToString(templateFile, "UTF-8");
		for (String key : replaceMap.keySet()) {
			String value = replaceMap.get(key);
			content = content.replaceAll(key, value);
		}
		File fileDir = new File(javaDir, daoPackagePath.replaceAll("\\.", "\\"+File.separator));
		fileDir.mkdirs();
		File classFile = new File(fileDir, daoClassName+".java");
		FileUtils.writeStringToFile(classFile, content, "UTF-8");
	}

	public void generateDaoXml(Map<String, Object> param, HgoTableInfo hgoTableInfo,
			List<HgoTableColumnInfo> columnList) throws IOException {
		String domain = param.get("domain").toString();
		String primaryKeyType = param.get("primaryKeyType").toString();
		String projectPath = param.get("projectPath").toString();
		File generatorDir = new File(projectPath);
		String mode = param.get("mode").toString();
		File javaDir = new File(generatorDir, "src/main/java");
		javaDir.mkdirs();
		String packageName = param.get("packageName").toString();
		String projectCode = HgoStringUtils.toUnderlineName(packageName);
		String basePackage = "cn.javaex.hgo.action." + projectCode + ".";
		if ("plugin".equals(mode)) {
			basePackage = "cn.javaex.hgo.plugin." + projectCode + ".";
		}
		Map<String, String> replaceMap = new HashMap<String, String>();
		String daoPackagePath = basePackage + "dao." + hgoTableInfo.getTableName();
		if ("plugin".equals(mode)) {
			daoPackagePath = basePackage + "dao";
		}
		replaceMap.put("#DAOPackage#", daoPackagePath);
		String entityPackage = basePackage + "view";
		replaceMap.put("#EntityPackage#", entityPackage);
		String entityClassName = HgoStringUtils.toCapitalizeCamelCase(hgoTableInfo.getTableName());
		replaceMap.put("#EntityClassName#", entityClassName);
		String entityObjectName = HgoStringUtils.initLower(entityClassName);
		replaceMap.put("#EntityObjectName#", entityObjectName);
		String daoClassName = "I" + entityClassName + "DAO";
		replaceMap.put("#DAOClassName#", daoClassName);
		for (HgoTableColumnInfo hgoTableColumnInfo : columnList) {
			if (hgoTableColumnInfo.getIsPrimaryKey()==1) {
				replaceMap.put("#ColumnIdName#", hgoTableColumnInfo.getColumnName());
				replaceMap.put("#ColumnIdPropertyName#", HgoStringUtils.toCamelCase(hgoTableColumnInfo.getColumnName()));
				break;
			}
		}
		replaceMap.put("#TableName#", hgoTableInfo.getTableName());
		StringBuffer resultMapList = new StringBuffer();
		StringBuffer columnNameList = new StringBuffer();
		StringBuffer propertyNameList = new StringBuffer();
		StringBuffer updateSetList = new StringBuffer();
		for (HgoTableColumnInfo hgoTableColumnInfo : columnList) {
			String columnName = hgoTableColumnInfo.getColumnName();
			String propertyName = HgoStringUtils.toCamelCase(columnName);
			if (StringUtils.isNotEmpty(resultMapList.toString())) {
				resultMapList.append(System.lineSeparator());
			}
			resultMapList.append("		<result column=\""+columnName+"\" property=\""+propertyName+"\" />");
			if (hgoTableColumnInfo.getIsPrimaryKey()==1 && "AUTO_INCREMENT".equals(primaryKeyType)) {
			} else {
				if (StringUtils.isNotEmpty(columnNameList.toString())) {
					columnNameList.append(System.lineSeparator());
				}
				columnNameList.append("			<if test=\""+propertyName+"!=null\">"+columnName+",</if>");
				
				if (StringUtils.isNotEmpty(propertyNameList.toString())) {
					propertyNameList.append(System.lineSeparator());
				}
				propertyNameList.append("			<if test=\""+propertyName+"!=null\">#{"+propertyName+"},</if>");
			}
			if (hgoTableColumnInfo.getIsPrimaryKey()!=1) {
				if (StringUtils.isNotEmpty(updateSetList.toString())) {
					updateSetList.append(System.lineSeparator());
				}
				updateSetList.append("			<if test=\""+propertyName+"!=null\">"+columnName+"=#{"+propertyName+"},</if>");
			}
		}
		replaceMap.put("#ResultMapList#", resultMapList.toString());
		replaceMap.put("#ColumnNameList#", columnNameList.toString());
		replaceMap.put("#PropertyNameList#", propertyNameList.toString());
		replaceMap.put("#UpdateSetList#", updateSetList.toString());
		String templateUrl = domain + HgoConstant.GENERATE_TEMPLATE;
		File templateFile = new File(templateUrl, "DAOMapperTemplate.xml");
		String content = FileUtils.readFileToString(templateFile, "UTF-8");
		for (String key : replaceMap.keySet()) {
			String value = replaceMap.get(key);
			content = content.replaceAll(key, value);
		}
		File fileDir = new File(javaDir, daoPackagePath.replaceAll("\\.", "\\"+File.separator));
		fileDir.mkdirs();
		File classFile = new File(fileDir, daoClassName+".xml");
		FileUtils.writeStringToFile(classFile, content, "UTF-8");
	}
	
	public void generateView(Map<String, Object> param, HgoTableInfo hgoTableInfo,
			List<HgoTableColumnInfo> columnList) throws IOException {
		String domain = param.get("domain").toString();
		String projectPath = param.get("projectPath").toString();
		File generatorDir = new File(projectPath);
		String mode = param.get("mode").toString();
		File javaDir = new File(generatorDir, "src/main/java");
		javaDir.mkdirs();
		String packageName = param.get("packageName").toString();
		String projectCode = HgoStringUtils.toUnderlineName(packageName);
		String basePackage = "cn.javaex.hgo.action." + projectCode + ".";
		if ("plugin".equals(mode)) {
			basePackage = "cn.javaex.hgo.plugin." + projectCode + ".";
		}
		Map<String, String> replaceMap = new HashMap<String, String>();
		String entityPackage = basePackage + "view";
		replaceMap.put("#EntityPackage#", entityPackage);
		String entityClassName = HgoStringUtils.toCapitalizeCamelCase(hgoTableInfo.getTableName());
		replaceMap.put("#EntityClassName#", entityClassName);
		StringBuffer imports = new StringBuffer();
		imports.append(System.lineSeparator());
		StringBuffer propertyDefinitions = new StringBuffer();
		StringBuffer propertyGetterSetters = new StringBuffer();
		for (HgoTableColumnInfo hgoTableColumnInfo : columnList) {
			String propertyType = "String";
			if (hgoTableColumnInfo.getIsPrimaryKey()!=1) {
				propertyType = changeType(hgoTableColumnInfo.getType());
				if (propertyType.equals("Date")) {
					if (imports.toString().indexOf("java.util.Date")==-1) {
						imports.append("import java.util.Date;"+System.lineSeparator());
					}
				} else if (propertyType.equals("BigDecimal")) {
					if (imports.toString().indexOf("java.math.BigDecimal")==-1) {
						imports.append("import java.math.BigDecimal;"+System.lineSeparator());
					}
				}
			}
			String propertyName = HgoStringUtils.toCamelCase(hgoTableColumnInfo.getColumnName());
			propertyDefinitions.append("	private "+propertyType+" "+propertyName+";");
			if (StringUtils.isNotEmpty(hgoTableColumnInfo.getChsname())) {
				propertyDefinitions.append("		// "+hgoTableColumnInfo.getChsname());
			}
			propertyDefinitions.append(System.lineSeparator());
			String propertyCapName = HgoStringUtils.initCap(propertyName);
			propertyGetterSetters.append("	/**"+System.lineSeparator());
			propertyGetterSetters.append("	 * 得到"+hgoTableColumnInfo.getChsname()+System.lineSeparator());
			propertyGetterSetters.append("	 * @return"+System.lineSeparator());
			propertyGetterSetters.append("	 */"+System.lineSeparator());
			propertyGetterSetters.append("	public "+propertyType+" get"+propertyCapName+"() {"+System.lineSeparator());
			propertyGetterSetters.append("		return "+propertyName+";"+System.lineSeparator());
			propertyGetterSetters.append("	}"+System.lineSeparator());
			propertyGetterSetters.append("	/**"+System.lineSeparator());
			propertyGetterSetters.append("	 * 设置"+hgoTableColumnInfo.getChsname()+System.lineSeparator());
			propertyGetterSetters.append("	 * @param "+propertyName+System.lineSeparator());
			propertyGetterSetters.append("	 */"+System.lineSeparator());
			propertyGetterSetters.append("	public void set"+propertyCapName+"("+propertyType+" "+propertyName+") {"+System.lineSeparator());
			propertyGetterSetters.append("		this."+propertyName+" = "+propertyName+";"+System.lineSeparator());
			propertyGetterSetters.append("	}"+System.lineSeparator());
			propertyGetterSetters.append(System.lineSeparator());
		}
		replaceMap.put("#imports#", imports.toString());
		if (StringUtils.isEmpty(hgoTableInfo.getChsname())) {
			replaceMap.put("#TableChsName#", "");
		} else {
			replaceMap.put("#TableChsName#", hgoTableInfo.getChsname());
		}
		replaceMap.put("#PropertyDefinitions#", propertyDefinitions.toString());
		replaceMap.put("#PropertyGetterSetters#", propertyGetterSetters.toString());
		String templateUrl = domain + HgoConstant.GENERATE_TEMPLATE;
		File templateFile = new File(templateUrl, "ViewTemplate.java");
		String content = FileUtils.readFileToString(templateFile, "UTF-8");
		for (String key : replaceMap.keySet()) {
			String value = replaceMap.get(key);
			content = content.replaceAll(key, value);
		}
		File fileDir = new File(javaDir, entityPackage.replaceAll("\\.", "\\"+File.separator));
		fileDir.mkdirs();
		File classFile = new File(fileDir, entityClassName+".java");
		FileUtils.writeStringToFile(classFile, content, "UTF-8");
	}
	
	public void generateJspList(Map<String, Object> param, HgoTableInfo hgoTableInfo,
			List<HgoTableColumnInfo> columnList) throws IOException {
		String domain = param.get("domain").toString();
		String projectPath = param.get("projectPath").toString();
		File generatorDir = new File(projectPath);
		String mode = param.get("mode").toString();
		String packageName = param.get("packageName").toString();
		File jspDir = null;
		if ("business".equals(mode)) {
			jspDir = new File(generatorDir, HgoConstant.GENERATE_JSP+packageName.replace(".", "/"));
		} else {
			jspDir = new File(generatorDir, HgoConstant.GENERATE_JSP+"plugin/"+packageName.replace(".", "/"));
		}
		jspDir.mkdirs();
		String tableName = hgoTableInfo.getTableName();
		Map<String, String> replaceMap = new HashMap<String, String>();
		String tableChsname = hgoTableInfo.getChsname();
		if (StringUtils.isEmpty(tableChsname)) {
			tableChsname = tableName;
		}
		replaceMap.put("#tableChsname#", tableChsname.replace("表", ""));
		StringBuffer jspTableHeaders = new StringBuffer();
		StringBuffer jspTableCells = new StringBuffer();
		int jspTableHeaderCount = 3;
		for (HgoTableColumnInfo hgoTableColumnInfo : columnList) {
			String propertyName = HgoStringUtils.toCamelCase(hgoTableColumnInfo.getColumnName());
			if (hgoTableColumnInfo.getIsPrimaryKey()==1) {
				replaceMap.put("#ColumnIdPropertyName#", propertyName);
			} else {
				String thHeader = hgoTableColumnInfo.getChsname();
				if (StringUtils.isEmpty(thHeader)) {
					thHeader = hgoTableColumnInfo.getColumnName();
				}
				if (StringUtils.isNotEmpty(jspTableHeaders.toString())) {
					jspTableHeaders.append(System.lineSeparator());
				}
				jspTableHeaders.append("									<th>"+thHeader+"</th>");
				if (StringUtils.isNotEmpty(jspTableCells.toString())) {
					jspTableCells.append(System.lineSeparator());
				}
				jspTableCells.append("												<td>\\$\\{entity\\."+propertyName+"\\}</td>");
				jspTableHeaderCount++;
			}
		}
		replaceMap.put("#JspTableHeaders#", jspTableHeaders.toString());
		replaceMap.put("#JspTableCells#", jspTableCells.toString());
		replaceMap.put("#JspTableHeaderCount#", String.valueOf(jspTableHeaderCount));
		String templateUrl = domain + HgoConstant.GENERATE_TEMPLATE;
		File templateFile = null;
		String isPage = param.get("isPage").toString();
		String editType = param.get("editType").toString();
		if ("1".equals(isPage)) {
			if ("popup".equals(editType)) {
				templateFile = new File(templateUrl, "JspListTemplate_page_dialog.jsp");
			} else {
				templateFile = new File(templateUrl, "JspListTemplate_page_jump.jsp");
			}
		} else {
			if ("popup".equals(editType)) {
				templateFile = new File(templateUrl, "JspListTemplate_nopage_dialog.jsp");
			} else {
				templateFile = new File(templateUrl, "JspListTemplate_nopage_jump.jsp");
			}
		}
		String content = FileUtils.readFileToString(templateFile, "UTF-8");
		for (String key : replaceMap.keySet()) {
			String value = replaceMap.get(key);
			content = content.replaceAll(key, value);
		}
		File fileDir = null;
		if ("business".equals(mode)) {
			fileDir = new File(jspDir, tableName);
			fileDir.mkdirs();
		} else {
			fileDir = jspDir;
		}
		File listFile = new File(fileDir, tableName+"_list.jsp");
		FileUtils.writeStringToFile(listFile, content, "UTF-8");
	}

	public void generateJspEdit(Map<String, Object> param, HgoTableInfo hgoTableInfo,
			List<HgoTableColumnInfo> columnList) throws IOException {
		String domain = param.get("domain").toString();
		String editType = param.get("editType").toString();
		String projectPath = param.get("projectPath").toString();
		File generatorDir = new File(projectPath);
		String mode = param.get("mode").toString();
		String packageName = param.get("packageName").toString();
		File jspDir = null;
		if ("business".equals(mode)) {
			jspDir = new File(generatorDir, HgoConstant.GENERATE_JSP+packageName.replace(".", "/"));
		} else {
			jspDir = new File(generatorDir, HgoConstant.GENERATE_JSP+"plugin/"+packageName.replace(".", "/"));
		}
		jspDir.mkdirs();
		String tableName = hgoTableInfo.getTableName();
		Map<String, String> replaceMap = new HashMap<String, String>();
		String tableChsname = hgoTableInfo.getChsname();
		if (StringUtils.isEmpty(tableChsname)) {
			tableChsname = tableName;
		}
		replaceMap.put("#tableChsname#", tableChsname.replace("表", ""));
		String entityClassName = HgoStringUtils.toCapitalizeCamelCase(hgoTableInfo.getTableName());
		String entityObjectName = HgoStringUtils.initLower(entityClassName);
		replaceMap.put("#EntityObjectName#", entityObjectName);
		StringBuffer jspEditFormFields = new StringBuffer();
		for (HgoTableColumnInfo hgoTableColumnInfo : columnList) {
			String propertyName = HgoStringUtils.toCamelCase(hgoTableColumnInfo.getColumnName());
			if (hgoTableColumnInfo.getIsPrimaryKey()==1) {
				replaceMap.put("#ColumnIdName#", hgoTableColumnInfo.getColumnName());
				replaceMap.put("#ColumnIdPropertyName#", propertyName);
			} else {
				String columnChsname = hgoTableColumnInfo.getChsname();
				if (StringUtils.isEmpty(columnChsname)) {
					columnChsname = hgoTableColumnInfo.getColumnName();
				}
				if ("popup".equals(editType)) {
					jspEditFormFields.append(System.lineSeparator());
					jspEditFormFields.append("			<!--"+columnChsname+"-->"+System.lineSeparator());
					jspEditFormFields.append("			<div class=\"unit\">"+System.lineSeparator());
					if (hgoTableColumnInfo.getIsNotNull()==1) {
						jspEditFormFields.append("				<div class=\"left\"><span class=\"required\">*</span><p class=\"subtitle\">"+columnChsname+"</p></div>"+System.lineSeparator());
					} else {
						jspEditFormFields.append("				<div class=\"left\"><p class=\"subtitle\">"+columnChsname+"</p></div>"+System.lineSeparator());
					}
					jspEditFormFields.append("				<div class=\"right\">"+System.lineSeparator());
					if (hgoTableColumnInfo.getIsNotNull()==1) {
						jspEditFormFields.append("					<input type=\"text\" class=\"text\" name=\""+propertyName+"\" data-type=\"必填\" value=\"\\$\\{"+entityObjectName+"\\."+propertyName+"\\}\" autocomplete=\"off\"/>"+System.lineSeparator());
					} else {
						jspEditFormFields.append("					<input type=\"text\" class=\"text\" name=\""+propertyName+"\" value=\"\\$\\{"+entityObjectName+"\\."+propertyName+"\\}\" autocomplete=\"off\"/>"+System.lineSeparator());
					}
					jspEditFormFields.append("				</div>"+System.lineSeparator());
					jspEditFormFields.append("				<span class=\"clearfix\"></span>"+System.lineSeparator());
					jspEditFormFields.append("			</div>"+System.lineSeparator());
				} else {
					jspEditFormFields.append(System.lineSeparator());
					jspEditFormFields.append("							<!--"+columnChsname+"-->"+System.lineSeparator());
					jspEditFormFields.append("							<div class=\"unit\">"+System.lineSeparator());
					if (hgoTableColumnInfo.getIsNotNull()==1) {
						jspEditFormFields.append("								<div class=\"left\"><span class=\"required\">*</span><p class=\"subtitle\">"+columnChsname+"</p></div>"+System.lineSeparator());
					} else {
						jspEditFormFields.append("								<div class=\"left\"><p class=\"subtitle\">"+columnChsname+"</p></div>"+System.lineSeparator());
					}
					jspEditFormFields.append("								<div class=\"right\">"+System.lineSeparator());
					if (hgoTableColumnInfo.getIsNotNull()==1) {
						jspEditFormFields.append("									<input type=\"text\" class=\"text\" name=\""+propertyName+"\" data-type=\"必填\" value=\"\\$\\{"+entityObjectName+"\\."+propertyName+"\\}\" autocomplete=\"off\"/>"+System.lineSeparator());
					} else {
						jspEditFormFields.append("									<input type=\"text\" class=\"text\" name=\""+propertyName+"\" value=\"\\$\\{"+entityObjectName+"\\."+propertyName+"\\}\" autocomplete=\"off\"/>"+System.lineSeparator());
					}
					jspEditFormFields.append("								</div>"+System.lineSeparator());
					jspEditFormFields.append("								<span class=\"clearfix\"></span>"+System.lineSeparator());
					jspEditFormFields.append("							</div>"+System.lineSeparator());
				}
			}
		}
		replaceMap.put("#JspEditFormFields#", jspEditFormFields.toString());
		String templateUrl = domain + HgoConstant.GENERATE_TEMPLATE;
		File templateFile = null;
		if ("popup".equals(editType)) {
			templateFile = new File(templateUrl, "JspEditTemplate_dialog.jsp");
		} else {
			templateFile = new File(templateUrl, "JspEditTemplate_jump.jsp");
		}
		String content = FileUtils.readFileToString(templateFile, "UTF-8");
		for (String key : replaceMap.keySet()) {
			String value = replaceMap.get(key);
			content = content.replaceAll(key, value);
		}
		File fileDir = null;
		if ("business".equals(mode)) {
			fileDir = new File(jspDir, tableName);
			fileDir.mkdirs();
		} else {
			fileDir = jspDir;
		}
		File listFile = new File(fileDir, tableName+"_edit.jsp");
		FileUtils.writeStringToFile(listFile, content, "UTF-8");
	}
}