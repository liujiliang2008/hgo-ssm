package cn.javaex.hgo.action.system.dao.hgo_table_info;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import cn.javaex.hgo.action.system.view.HgoTableInfo;

public interface IHgoTableInfoDAO {

	/**
	 * 查询列表
	 * @param param
	 * @return
	 */
	List<HgoTableInfo> list(Map<String, Object> param);

	/**
	 * 根据主键，查询表信息
	 * @param id
	 * @return
	 */
	HgoTableInfo selectById(String id);

	/**
	 * 插入
	 * @param hgoTableInfo
	 * @return
	 */
	int insert(HgoTableInfo hgoTableInfo);

	/**
	 * 更新
	 * @param hgoTableInfo
	 * @return
	 */
	int update(HgoTableInfo hgoTableInfo);

	/**
	 * 删除记录
	 * @param id
	 * @return
	 */
	int deleteById(String id);
	
	/**
	 * 校验表名是否重复
	 * @param tableName 表名
	 * @param id 主键
	 * @return
	 */
	int countByTableName(@Param("tableName") String tableName, @Param("id") String id);

	/**
	 * 校验表在数据库中是否已存在
	 * @param tableName 表名
	 * @return
	 */
	int isTableExist(@Param("tableName") String tableName);

	/**
	 * 创建表
	 * @param alterSql
	 */
	void alter(@Param("alterSql") String alterSql);

	/**
	 * 获取该表在数据库中的所有字段
	 * @param tableName 表名
	 * @return
	 */
	Set<String> listColumnByTableName(@Param("tableName") String tableName);

	/**
	 * 数据库表列表
	 * @param param
	 * @return
	 */
	List<String> listTableFromDatabase(Map<String, Object> param);

	/**
	 * 查询表的字段信息
	 * @param tableName 表名
	 * @return
	 */
	List<Map<String, String>> listColumnInfoFromDatabaseByTableName(@Param("tableName") String tableName);
}