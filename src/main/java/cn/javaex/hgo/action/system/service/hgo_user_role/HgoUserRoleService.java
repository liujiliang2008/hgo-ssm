package cn.javaex.hgo.action.system.service.hgo_user_role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.javaex.hgo.action.system.dao.hgo_user_role.IHgoUserRoleDAO;
import cn.javaex.hgo.config.shiro.HgoRealm;

@Service("HgoUserRoleService")
public class HgoUserRoleService {
	
	@Autowired
	private IHgoUserRoleDAO iHgoUserRoleDAO;
	@Autowired
	private HgoRealm hgoRealm;
	
	/**
	 * 向角色添加用户
	 * @param roleId 角色id
	 * @param userIdArr 用户id数组
	 */
	public void save(String roleId, String[] userIdArr) {
		iHgoUserRoleDAO.insertUser(roleId, userIdArr);
	}

	/**
	 * 移除用户
	 * @param roleId 角色id
	 * @param userId 用户id
	 */
	public void deleteUser(String roleId, String userId) {
		// 1.0 移除用户
		iHgoUserRoleDAO.deleteUser(roleId, userId);
		
		// 2.0 清除用户授权缓存
		hgoRealm.clearAuthorizationInfo(userId);
	}

	/**
	 * 批量移除用户
	 * @param roleId 角色id
	 * @param userIdArr 用户id数组
	 */
	public void batchDeleteUser(String roleId, String[] userIdArr) {
		// 1.0 移除用户
		iHgoUserRoleDAO.batchDeleteUser(roleId, userIdArr);
		
		// 2.0 清除用户授权缓存
		for (String userId : userIdArr) {
			hgoRealm.clearAuthorizationInfo(userId);
		}
	}

}