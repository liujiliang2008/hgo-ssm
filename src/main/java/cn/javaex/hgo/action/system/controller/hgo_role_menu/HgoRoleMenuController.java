package cn.javaex.hgo.action.system.controller.hgo_role_menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.javaex.hgo.action.system.service.hgo_role_menu.HgoRoleMenuService;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("hgo_role_menu")
public class HgoRoleMenuController {

	@Autowired
	private HgoRoleMenuService hgoRoleMenuService;
	
	/**
	 * 保存角色菜单的分配
	 * @return
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public Result save(
			@RequestParam(value="roleId") String roleId,
			@RequestParam(required=false, value="menuIdArr") String[] menuIdArr) {
		
		hgoRoleMenuService.save(roleId, menuIdArr);
		
		return Result.success();
	}
	
}
