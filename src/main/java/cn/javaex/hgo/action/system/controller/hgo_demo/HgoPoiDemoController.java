package cn.javaex.hgo.action.system.controller.hgo_demo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.javaex.hgo.action.system.service.hgo_demo.HgoPoiDemoService;
import cn.javaex.hgo.action.system.view.HgoPoiDemo;
import cn.javaex.hgo.util.HgoPoiUtils;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("hgo_poi_demo")
public class HgoPoiDemoController {

	@Autowired
	private HgoPoiDemoService hgoPoiDemoService;
	
	/**
	 * 导出
	 * @param request 必须
	 * @param response 必须
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("exportExcel.action")
	public String exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		hgoPoiDemoService.exportExcelXlsx();
		
		return null;
	}
	
	/**
	 * 下载导入模板
	 * @param request 必须
	 * @param response 必须
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("exportTemplate.action")
	public String exportTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		hgoPoiDemoService.exportTemplate();
		
		return null;
	}
	
	/**
	 * 跳转导入页面
	 * @return
	 */
	@RequestMapping("importExcel.action")
	public String importExcel() {
		return "system/hgo_user_info/importExcel";
	}
	
	/**
	 * 导入Excel
	 * @param file
	 * @param request 必须
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("importExcel.json")
	@ResponseBody
	public Result importExcel(MultipartFile file, HttpServletRequest request) throws Exception {
		/**
		 * 导入方法一：自己逐行逐个字段取值并判断
		 * 有点：速度快
		 * 缺点：如果后期维护需要再导入一个字段，并且是在中间插入，那么整个顺序都得重写
		 */
		String errorMsg = hgoPoiDemoService.importExcel(file);
		
		/**
		 * 导入方法二：利用hgo提供的工具类，将每一行转为一个实体对象，整个excel是一个list
		 * 有点：不再需要考虑顺序问题
		 * 缺点：速度比方法一慢（数据量小可以忽略）；如果数据量非常庞大，几万、十几万，那么速度（未测试）
		 */
//		List<HgoPoiDemo> list = HgoPoiUtils.excelToEntity(file.getInputStream(), HgoPoiDemo.class);
//		String errorMsg = "";
//		if (list!=null && list.isEmpty()==false) {
//			errorMsg = hgoPoiDemoService.importExcelEx(list);
//		}
		
		return Result.success().add("errorMsg", errorMsg);
	}

}
