package cn.javaex.hgo.action.system.dao.hgo_table_column_info;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.javaex.hgo.action.system.view.HgoTableColumnInfo;

public interface IHgoTableColumnInfoDAO {

	/**
	 * 查询表的字段
	 * @param tableId 表的主键
	 * @return
	 */
	List<HgoTableColumnInfo> listByTableId(String tableId);

	/**
	 * 根据主键，查询表信息
	 * @param id
	 * @return
	 */
	HgoTableColumnInfo selectById(String id);

	/**
	 * 插入
	 * @param hgoTableColumnInfo
	 * @return
	 */
	int insert(HgoTableColumnInfo hgoTableColumnInfo);

	/**
	 * 更新
	 * @param hgoTableColumnInfo
	 * @return
	 */
	int update(HgoTableColumnInfo hgoTableColumnInfo);

	/**
	 * 删除记录
	 * @param id
	 * @return
	 */
	int deleteById(String id);

	/**
	 * 校验字段名是否重复
	 * @param tableId 表id
	 * @param columnName 字段名
	 * @param id 主键
	 * @return
	 */
	int countByColumnName(@Param("tableId") String tableId, @Param("columnName") String columnName, @Param("id") String id);

	/**
	 * 查询当前表下最大排序
	 * @param tableId
	 * @return
	 */
	int selectMaxSortByTableId(String tableId);

	/**
	 * 根据表主键删除字段表中的数据
	 * @param tableId 表id
	 * @return
	 */
	int deleteByTableId(String tableId);
	
}