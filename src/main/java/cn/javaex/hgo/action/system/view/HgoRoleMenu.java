package cn.javaex.hgo.action.system.view;

/**
 * 角色菜单表（为用户配置多个角色，每个角色下有各自权限的菜单）
 * 
 * @author 陈霓清
 */
public class HgoRoleMenu {
	private String roleId;	// 角色id
	private String menuId;	// 菜单id
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	
}
