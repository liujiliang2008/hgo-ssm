package cn.javaex.hgo.action.system.view;

import java.util.List;
import java.util.Set;

/**
 * 用户表
 * 
 * @author 陈霓清
 */
public class HgoUserInfo {
	private String id;			// 主键
	private String loginName;	// 账号
	private String password;	// 密码
	private String username;	// 真实姓名
	private String type;		// 用户类型
	private String email;		// 用户邮箱
	private String phone;		// 用户手机号
	private String avatar;		// 用户头像
	private Integer status;		// 状态（1：正常， 2：禁止登录）
	
	private List<HgoMenuInfo> menuList;	// 用户菜单信息
	private Set<String> menuUrlSet;		// 用户具有的菜单访问路径
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<HgoMenuInfo> getMenuList() {
		return menuList;
	}
	public void setMenuList(List<HgoMenuInfo> menuList) {
		this.menuList = menuList;
	}
	public Set<String> getMenuUrlSet() {
		return menuUrlSet;
	}
	public void setMenuUrlSet(Set<String> menuUrlSet) {
		this.menuUrlSet = menuUrlSet;
	}
	
}
