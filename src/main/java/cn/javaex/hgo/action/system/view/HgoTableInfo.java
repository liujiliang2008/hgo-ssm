package cn.javaex.hgo.action.system.view;

import java.util.Date;

/**
 * 表信息
 * 
 * @author 陈霓清
 */
public class HgoTableInfo {
	private String id;			// 主键
	private String tableName;	// 表名
	private String chsname;		// 描述
	private String remark;		// 备注
	private Date createTime;	// 创建时间
	private Date updateTime;	// 最后修改时间
	private Integer status;		// 启用状态（1：启用， 0：废弃）
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getChsname() {
		return chsname;
	}
	public void setChsname(String chsname) {
		this.chsname = chsname;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
