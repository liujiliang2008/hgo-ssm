package cn.javaex.hgo.action.system.dao.hgo_role_info;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import cn.javaex.hgo.action.system.view.HgoRoleInfo;

public interface IHgoRoleInfoDAO {

	/**
	 * 获取所有角色
	 * @return
	 */
	List<HgoRoleInfo> list();

	/**
	 * 新建角色
	 * @param hgoRoleInfo
	 * @return
	 */
	int insert(HgoRoleInfo hgoRoleInfo);

	/**
	 * 更新角色
	 * @param hgoRoleInfo
	 * @return
	 */
	int update(HgoRoleInfo hgoRoleInfo);

	/**
	 * 根据主键查询角色
	 * @param id 角色id
	 * @return
	 */
	HgoRoleInfo selectById(String id);

	/**
	 * 查询目前最大排序
	 * @return
	 */
	int selectMaxSort();

	/**
	 * 删除角色
	 * @param id 角色id
	 * @return
	 */
	int deleteById(String id);

	/**
	 * 校验角色标识是否重复
	 * @param code 角色标识
	 * @param id 主键
	 * @return
	 */
	int countByCode(@Param("code") String code, @Param("id") String id);

	/**
	 * 根据角色id list，查询角色名称
	 * @param idList
	 * @return
	 */
	List<String> listNameByIdList(@Param("idList") List<Integer> idList);

	/**
	 * 根据用户id，查询用户有哪些角色（只查询code）
	 * @param userId 用户id
	 * @return
	 */
	Set<String> listCodeByUserId(@Param("userId") String userId);
	
}