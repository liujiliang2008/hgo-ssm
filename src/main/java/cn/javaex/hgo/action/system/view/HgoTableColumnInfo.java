package cn.javaex.hgo.action.system.view;

import java.util.Date;

/**
 * 表字段
 * 
 * @author 陈霓清
 */
public class HgoTableColumnInfo {
	private String id;				// 主键
	private String tableId;			// 表id
	private String columnName;		// 字段名
	private String chsname;			// 字段表述
	private String type;			// 字段类型
	private Integer length;			// 字段长度
	private Integer point;			// 小数点
	private Integer isPrimaryKey;	// 该字段是否是主键
	private Integer isNotNull;		// 该字段是否不允许空值
	private String defaultValue;	// 默认值
	private String remark;			// 备注
	private Date createTime;		// 创建时间
	private Date updateTime;		// 最后修改时间
	private Integer status;			// 启用状态（1：启用， 0：废弃）
	private Integer sort;			// 排序
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getChsname() {
		return chsname;
	}
	public void setChsname(String chsname) {
		this.chsname = chsname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public Integer getPoint() {
		return point;
	}
	public void setPoint(Integer point) {
		this.point = point;
	}
	public Integer getIsPrimaryKey() {
		return isPrimaryKey;
	}
	public void setIsPrimaryKey(Integer isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}
	public Integer getIsNotNull() {
		return isNotNull;
	}
	public void setIsNotNull(Integer isNotNull) {
		this.isNotNull = isNotNull;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}
