package cn.javaex.hgo.action.system.controller.hgo_table_info;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.javaex.hgo.action.system.service.hgo_table_info.HgoTableInfoService;
import cn.javaex.hgo.action.system.view.HgoTableInfo;
import cn.javaex.hgo.config.exception.HgoException;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("hgo_table_info")
public class HgoTableInfoController {

	@Autowired
	private HgoTableInfoService hgoTableInfoService;
	
	/**
	 * 查询列表
	 * @param map
	 * @param keyword
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("list.action")
	public String list(ModelMap map,
			@RequestParam(required=false, value="keyword") String keyword,
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize) {
		Map<String, Object> param = new HashMap<String, Object>();
		if (StringUtils.isNotBlank(keyword)) {
			param.put("keyword", keyword.trim());
			map.put("keyword", keyword);
		}
		
		// pageHelper分页插件
		// 只需要在查询之前调用，传入当前页码，以及每一页显示多少条
		PageHelper.startPage(pageNum, pageSize);
		List<HgoTableInfo> list = hgoTableInfoService.list(param);
		PageInfo<HgoTableInfo> pageInfo = new PageInfo<HgoTableInfo>(list);
		map.put("pageInfo", pageInfo);
		
		return "system/hgo_table_info/hgo_table_info_list";
	}
	
	/**
	 * 表编辑
	 * @param map
	 * @param id 主键
	 * @return
	 */
	@RequestMapping("edit.action")
	public String edit(ModelMap map,
			@RequestParam(required=false, value="id") String id) {
		
		// 查询单个表的信息
		if (StringUtils.isNotEmpty(id)) {
			HgoTableInfo hgoTableInfo = hgoTableInfoService.selectById(id);
			map.put("hgoTableInfo", hgoTableInfo);
		}
		
		return "system/hgo_table_info/hgo_table_info_edit";
	}
	
	/**
	 * 保存
	 * @param hgoTableInfo
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public Result save(HgoTableInfo hgoTableInfo) throws HgoException {
		
		hgoTableInfoService.save(hgoTableInfo);
		
		return Result.success();
	}
	
	/**
	 * 删除表
	 * @param id
	 * @return
	 */
	@RequestMapping("delete.json")
	@ResponseBody
	public Result delete(@RequestParam(value="id") String id) {
		
		hgoTableInfoService.deleteById(id);
		
		return Result.success();
	}
	
	/**
	 * 同步数据库表
	 * @param idArr 表主键数组
	 * @param synchronousType 同步类型（1：普通同步；2：强制同步（先删除已存在的表，再创建））
	 * @return
	 */
	@RequestMapping("synchronousToDatabase.json")
	@ResponseBody
	public Result synchronousToDatabase(
			@RequestParam(value="idArr") String[] idArr,
			@RequestParam(value="synchronousType") String synchronousType) {
		
		hgoTableInfoService.synchronousToDatabase(idArr, synchronousType);
		
		return Result.success();
	}
	
	/**
	 * 数据库表列表
	 * @param map
	 * @param keyword
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("listTableFromDatabase.action")
	public String listTableFromDatabase(ModelMap map,
			@RequestParam(required=false, value="keyword") String keyword,
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize) {
		Map<String, Object> param = new HashMap<String, Object>();
		if (StringUtils.isNotBlank(keyword)) {
			param.put("keyword", keyword.trim());
			map.put("keyword", keyword);
		}
		
		// pageHelper分页插件
		// 只需要在查询之前调用，传入当前页码，以及每一页显示多少条
		PageHelper.startPage(pageNum, pageSize);
		List<String> list = hgoTableInfoService.listTableFromDatabase(param);
		PageInfo<String> pageInfo = new PageInfo<String>(list);
		map.put("pageInfo", pageInfo);
		
		return "system/hgo_table_info/listTableFromDatabase";
	}
	
	/**
	 * 从数据库导出表信息
	 * @param tableNameArr 表名数组
	 * @return
	 */
	@RequestMapping("synchronousFromDatabase.json")
	@ResponseBody
	public Result synchronousFromDatabase(@RequestParam(value="tableNameArr") String[] tableNameArr) {
		
		hgoTableInfoService.synchronousFromDatabase(tableNameArr);
		
		return Result.success();
	}
	
	/**
	 * 跳转代码生成页面
	 * @param map
	 * @param idArrStr
	 * @return
	 */
	@RequestMapping("codeGenerate.action")
	public String codeGenerate(ModelMap map,
			@RequestParam(value="idArrStr") String idArrStr) {
		
		String tip = "";
		String[] idArr = idArrStr.split("_");
		for (String id : idArr) {
			HgoTableInfo hgoTableInfo = hgoTableInfoService.selectById(id);
			if (hgoTableInfo!=null) {
				if ("".equals(tip)) {
					tip = hgoTableInfo.getTableName();
				} else {
					tip += "、" + hgoTableInfo.getTableName();
				}
			}
		}
		
		map.put("idArrStr", idArrStr);
		map.put("tip", tip);
		return "system/hgo_table_info/codeGenerate";
	}
	/**
	 * 代码生成
	 * @param idArr
	 * @return
	 * @throws IOException
	 * @throws HgoException
	 */
	@RequestMapping("codeGenerate.json")
	@ResponseBody
	public Result codeGenerate(HttpServletRequest request) throws IOException, HgoException {
		Map<String, Object> param = new HashMap<String, Object>();
		// 应用程序根路径
		String domain = request.getSession().getServletContext().getRealPath("/");
		param.put("domain", domain);
		// 表id数组
		String idArrStr = request.getParameter("idArrStr");
		String[] idArr = idArrStr.split("_");
		param.put("idArr", idArr);
		// 项目路径
		String projectPath = request.getParameter("projectPath");
		param.put("projectPath", projectPath);
		// 包名
		String packageName = request.getParameter("packageName");
		param.put("packageName", packageName.trim().toLowerCase());
		// 主键策略
		String primaryKeyType = request.getParameter("primaryKeyType");
		param.put("primaryKeyType", primaryKeyType);
		// 是否分页
		String isPage = request.getParameter("isPage");
		param.put("isPage", isPage);
		// 生成代码种类
		String[] codeTypeArr = request.getParameterValues("codeType");
		param.put("codeTypeArr", codeTypeArr);
		// 编辑页类型
		String editType = request.getParameter("editType");
		param.put("editType", editType);
		// 开发场景
		String mode = request.getParameter("mode");
		param.put("mode", mode);
		
		hgoTableInfoService.codeGenerate(param);
		
		return Result.success();
	}
}
