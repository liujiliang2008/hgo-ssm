package cn.javaex.hgo.action.system.controller.hgo_table_column_info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.javaex.hgo.action.system.service.hgo_table_column_info.HgoTableColumnInfoService;
import cn.javaex.hgo.action.system.service.hgo_table_info.HgoTableInfoService;
import cn.javaex.hgo.action.system.view.HgoTableColumnInfo;
import cn.javaex.hgo.action.system.view.HgoTableInfo;
import cn.javaex.hgo.config.constant.HgoFixedOption.TableColumnType;
import cn.javaex.hgo.config.exception.HgoException;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("hgo_table_column_info")
public class HgoTableColumnInfoController {

	@Autowired
	private HgoTableColumnInfoService hgoTableColumnInfoService;
	@Autowired
	private HgoTableInfoService hgoTableInfoService;
	
	/**
	 * 查询所有字段
	 * @param map
	 * @return
	 */
	@RequestMapping("list.action")
	public String list(ModelMap map,
			@RequestParam(value="tableId") String tableId) {
		// 表信息
		HgoTableInfo hgoTableInfo = hgoTableInfoService.selectById(tableId);
		map.put("hgoTableInfo", hgoTableInfo);
		
		// 字段列表
		List<HgoTableColumnInfo> list = hgoTableColumnInfoService.listByTableId(tableId);
		map.put("list", list);
		
		// 字段类型
		Map<String, String> typeMap = new HashMap<String, String>();
		for (TableColumnType opt : TableColumnType.values()) {
			typeMap.put(opt.getValue(), opt.getName());
		}
		map.put("typeMap", typeMap);
		
		map.put("tableId", tableId);
		return "system/hgo_table_column_info/hgo_table_column_info_list";
	}
	
	/**
	 * 字段编辑
	 * @param map
	 * @param id 主键
	 * @return
	 */
	@RequestMapping("edit.action")
	public String edit(ModelMap map,
			@RequestParam(required=false, value="id") String id,
			@RequestParam(value="tableId") String tableId) {
		
		// 查询单个表的信息
		if (StringUtils.isNotEmpty(id)) {
			HgoTableColumnInfo hgoTableColumnInfo = hgoTableColumnInfoService.selectById(id);
			map.put("hgoTableColumnInfo", hgoTableColumnInfo);
		}
		
		// 查询当前表下最大排序
		int maxSort = hgoTableColumnInfoService.selectMaxSortByTableId(tableId);
		map.put("maxSort", maxSort+1);
		
		// 字段类型
		List<Map<String, String>> typeList = new ArrayList<Map<String, String>>();
		Map<String, String> typeMap = null;
		for (TableColumnType opt : TableColumnType.values()) {
			typeMap = new HashMap<String, String>();
			typeMap.put("value", opt.getValue());
			typeMap.put("name", opt.getName());
			typeList.add(typeMap);
		}
		map.put("typeList", typeList);
		
		map.put("tableId", tableId);
		return "system/hgo_table_column_info/hgo_table_column_info_edit";
	}
	
	/**
	 * 保存
	 * @param hgoTableColumnInfo
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public Result save(HgoTableColumnInfo hgoTableColumnInfo) throws HgoException {
		
		hgoTableColumnInfoService.save(hgoTableColumnInfo);
		
		return Result.success();
	}
	
	/**
	 * 删除字段
	 * @param id
	 * @return
	 */
	@RequestMapping("delete.json")
	@ResponseBody
	public Result delete(@RequestParam(value="id") String id) {
		
		hgoTableColumnInfoService.deleteById(id);
		
		return Result.success();
	}
	
	/**
	 * 保存排序
	 * @param idArr
	 * @param sortArr
	 * @return
	 */
	@RequestMapping("saveSort.json")
	@ResponseBody
	public Result saveSort(
			@RequestParam(value="idArr") String[] idArr,
			@RequestParam(value="sortArr") int[] sortArr) {
		
		List<HgoTableColumnInfo> hgoTableColumnList = new ArrayList<HgoTableColumnInfo>();
		
		// 遍历idArr数组
		for (int i=0; i<idArr.length; i++) {
			HgoTableColumnInfo hgoTableColumnInfo = new HgoTableColumnInfo();
			hgoTableColumnInfo.setId(idArr[i]);
			hgoTableColumnInfo.setSort(sortArr[i]);
			
			hgoTableColumnList.add(hgoTableColumnInfo);
		}
		
		hgoTableColumnInfoService.saveSort(hgoTableColumnList);
		
		return Result.success();
	}
}
