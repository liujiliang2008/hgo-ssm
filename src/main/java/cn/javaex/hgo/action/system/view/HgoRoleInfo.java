package cn.javaex.hgo.action.system.view;

/**
 * 角色表
 * 
 * @author 陈霓清
 */
public class HgoRoleInfo {
	private String id;		// 主键
	private String name;	// 角色名称
	private String code;	// 角色标识
	private Integer sort;	// 排序
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}
