package cn.javaex.hgo.action.system.dao.hgo_menu_info;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import cn.javaex.hgo.action.system.view.HgoMenuInfo;

public interface IHgoMenuInfoDAO {

	/**
	 * 获取所有菜单
	 * @return
	 */
	List<HgoMenuInfo> list();
	
	/**
	 * 根据主键，查询菜单
	 * @param id
	 * @return
	 */
	HgoMenuInfo selectById(String id);
	
	/**
	 * 根据父节点id，查询菜单列表
	 * @param parentId 父节点id
	 * @return
	 */
	List<HgoMenuInfo> listByParentId(String parentId);
	
	int insert(HgoMenuInfo hgoMenuInfo);

	int update(HgoMenuInfo hgoMenuInfo);

	/**
	 * 根据主键删除菜单
	 * @param id 菜单id
	 */
	int deleteById(String id);

	/**
	 * 查询当前菜单级别下最大排序
	 * @param parentId
	 * @return
	 */
	int selectMaxSortByParentId(String parentId);

	/**
	 * 查询当前用户有权限的菜单
	 * @param userId 用户id
	 * @return
	 */
	List<HgoMenuInfo> listByUserId(@Param("userId") String userId);

	/**
	 * 查询所有菜单路径
	 * @return
	 */
	Set<String> listMenuUrl();

	/**
	 * 查询所有权限标识
	 * @return
	 */
	Set<String> listPermCode();
	
	/**
	 * 校验按钮权限标识是否重复
	 * @param permCode 按钮权限标识
	 * @param id 主键
	 * @return
	 */
	int countByPermCode(@Param("permCode") String permCode, @Param("id") String id);

	/**
	 * 查询用户的所有按钮权限
	 * @param userId 用户id
	 * @return
	 */
	Set<String> listPermCodeByUserId(@Param("userId") String userId);

}