package cn.javaex.hgo.action.system.service.hgo_role_menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.javaex.hgo.action.system.dao.hgo_role_menu.IHgoRoleMenuDAO;
import cn.javaex.hgo.action.system.view.HgoRoleMenu;
import cn.javaex.hgo.config.shiro.HgoRealm;

@Service("HgoRoleMenuService")
public class HgoRoleMenuService {
	
	@Autowired
	private IHgoRoleMenuDAO iHgoRoleMenuDAO;
	@Autowired
	private HgoRealm hgoRealm;
	
	/**
	 * 保存角色菜单的分配
	 * @param roleId
	 * @param menuIdArr
	 */
	public void save(String roleId, String[] menuIdArr) {
		// 1.0 删除该角色的菜单
		iHgoRoleMenuDAO.deleteByRoleId(roleId);
		
		// 2.0 重新插入角色菜单
		if (menuIdArr!=null && menuIdArr.length>0) {
			for (int i=0; i<menuIdArr.length; i++) {
				HgoRoleMenu hgoRoleMenu = new HgoRoleMenu();
				hgoRoleMenu.setRoleId(roleId);
				hgoRoleMenu.setMenuId(menuIdArr[i]);
				
				iHgoRoleMenuDAO.insert(hgoRoleMenu);
			}
		}
		
		// 3.0 清空所有用户的授权认证缓存
		hgoRealm.clearAuthorizationInfo();
	}

}