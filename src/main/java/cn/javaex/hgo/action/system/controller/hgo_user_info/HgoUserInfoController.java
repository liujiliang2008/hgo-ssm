package cn.javaex.hgo.action.system.controller.hgo_user_info;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.javaex.hgo.action.system.service.hgo_user_info.HgoUserInfoService;
import cn.javaex.hgo.action.system.view.HgoUserInfo;
import cn.javaex.hgo.config.constant.HgoErrorMsg;
import cn.javaex.hgo.config.exception.HgoException;
import cn.javaex.hgo.util.HgoUtils;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("hgo_user_info")
public class HgoUserInfoController {

	@Autowired
	private HgoUserInfoService hgoUserInfoService;
	
	/**
	 * 查询用户列表
	 * @param map
	 * @param keyword
	 * @param status
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("list.action")
	public String list(ModelMap map,
			@RequestParam(required=false, value="keyword") String keyword,
			@RequestParam(required=false, value="status") String status,
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize) {
		
		Map<String, Object> param = new HashMap<String, Object>();
		if (StringUtils.isNotBlank(keyword)) {
			param.put("keyword", keyword.trim());
			map.put("keyword", keyword);
		}
		if (StringUtils.isNotEmpty(status)) {
			param.put("status", status);
			map.put("status", status);
		}
		
		// pageHelper分页插件
		// 只需要在查询之前调用，传入当前页码，以及每一页显示多少条
		PageHelper.startPage(pageNum, pageSize);
		List<HgoUserInfo> list = hgoUserInfoService.list(param);
		PageInfo<HgoUserInfo> pageInfo = new PageInfo<HgoUserInfo>(list);
		map.put("pageInfo", pageInfo);
		
		return "system/hgo_user_info/hgo_user_info_list";
	}
	
	/**
	 * 用户编辑
	 * @param map
	 * @param id 用户id
	 * @return
	 */
	@RequestMapping("edit.action")
	public String edit(ModelMap map,
			@RequestParam(required=false, value="id") String id) {
		
		// 查询单个用户的信息
		if (StringUtils.isNotEmpty(id)) {
			HgoUserInfo hgoUserInfo = hgoUserInfoService.selectById(id);
			map.put("hgoUserInfo", hgoUserInfo);
		}
		
		return "system/hgo_user_info/hgo_user_info_edit";
	}
	
	/**
	 * 保存
	 * @param hgoUserInfo
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public Result save(HgoUserInfo hgoUserInfo) throws HgoException {
		
		hgoUserInfoService.save(hgoUserInfo);
		
		return Result.success();
	}
	
	/**
	 * 删除用户
	 * @param id
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("delete.json")
	@ResponseBody
	public Result delete(@RequestParam(value="id") String id) throws HgoException {
		
		hgoUserInfoService.deleteById(id);
		
		return Result.success();
	}
	
	/**
	 * 获取某个角色下的用户列表
	 * @param roleId 角色id
	 * @return
	 */
	@RequestMapping("listUserByRole.json")
	@ResponseBody
	public Result listUserByRole(
			@RequestParam(value="roleId") String roleId,
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize) {
		
		// pageHelper分页插件
		// 只需要在查询之前调用，传入当前页码，以及每一页显示多少条
		PageHelper.startPage(pageNum, pageSize);
		List<HgoUserInfo> list = hgoUserInfoService.listByRoleId(roleId);
		PageInfo<HgoUserInfo> pageInfo = new PageInfo<HgoUserInfo>(list);
		
		return Result.success().add("pageInfo", pageInfo);
	}

	/**
	 * 跳转用户个人中心页面
	 * @return
	 */
	@RequestMapping("userCenter.action")
	public String userCenter() {
		return "system/hgo_user_info/userCenter";
	}
	
	/**
	 * 跳转修改头像页面
	 * @return
	 */
	@RequestMapping("changeAvatar.action")
	public String changeAvatar() {
		return "system/hgo_user_info/changeAvatar";
	}
	
	/**
	 * 保存头像
	 * @param avatar
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("saveAvatar.json")
	@ResponseBody
	public Result saveAvatar(@RequestParam(value="avatar") String avatar) throws HgoException {
		
		HgoUserInfo userInfo = HgoUtils.getUserInfo();
		userInfo.setAvatar(avatar);
		
		hgoUserInfoService.updateByUserSelf(userInfo);
		
		return Result.success();
	}
	
	/**
	 * 跳转修改密码页面
	 * @return
	 */
	@RequestMapping("changePassword.action")
	public String changePassword() {
		return "system/hgo_user_info/changePassword";
	}
	
	/**
	 * 修改密码
	 * @param oldPassword 原密码
	 * @param newPassword 新密码
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("savePassword.json")
	@ResponseBody
	public Result savePassword(
			@RequestParam(value="oldPassword") String oldPassword,
			@RequestParam(value="newPassword") String newPassword) throws HgoException {
		
		HgoUserInfo userInfo = HgoUtils.getUserInfo();
		// 加密后的旧密码
		String encryptOldPassword = new Md5Hash(oldPassword).toString();
		if (!encryptOldPassword.equals(userInfo.getPassword())) {
			throw new HgoException(HgoErrorMsg.ERROR_100012);
		}
		// 校验密码长度
		newPassword = newPassword.replaceAll("\\s*", "");
		if (newPassword.length()<6 || newPassword.length()>16) {
			throw new HgoException(HgoErrorMsg.ERROR_100004);
		}
		// 密码加密
		String encryptNewPassword = new Md5Hash(newPassword).toString();
		userInfo.setPassword(encryptNewPassword);
		
		hgoUserInfoService.updateByUserSelf(userInfo);
		
		return Result.success();
	}
}
