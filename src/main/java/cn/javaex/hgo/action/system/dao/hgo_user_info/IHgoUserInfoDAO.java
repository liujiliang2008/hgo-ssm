package cn.javaex.hgo.action.system.dao.hgo_user_info;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cn.javaex.hgo.action.system.view.HgoUserInfo;

public interface IHgoUserInfoDAO {

	/**
	 * 查询用户
	 * @param loginName
	 * @return
	 */
	HgoUserInfo selectByLoginName(String loginName);

	/**
	 *  查询用户列表
	 * @param param 
	 * @return
	 */
	List<HgoUserInfo> list(Map<String, Object> param);

	/**
	 * 根据主键，查询用户信息
	 * @param id
	 * @return
	 */
	HgoUserInfo selectById(String id);

	/**
	 * 根据登录名查询用户数量
	 * @param loginName
	 * @return
	 */
	int countByLoginName(String loginName);

	/**
	 * 插入
	 * @param hgoUserInfo
	 * @return
	 */
	int insert(HgoUserInfo hgoUserInfo);

	/**
	 * 更新
	 * @param hgoUserInfo
	 * @return
	 */
	int update(HgoUserInfo hgoUserInfo);

	/**
	 * 根据主键删除
	 * @param id
	 * @return
	 */
	int deleteById(String id);

	/**
	 * 获取某个角色下的用户列表
	 * @param roleId 角色id
	 * @return
	 */
	List<HgoUserInfo> listByRoleId(@Param("roleId") String roleId);

	/**
	 * 批量插入
	 * @param entitylist
	 * @return
	 */
	int batchInsert(List<HgoUserInfo> entitylist);

}