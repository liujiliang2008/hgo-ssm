package cn.javaex.hgo.action.system.view;

/**
 * 导入Excel（封装成实体对象），不需要在数据库中建立表
 * 
 * @author 陈霓清
 */
public class HgoPoiDemo {
	private String loginName;	// 账号
	private String username;	// 真实姓名
	private String email;		// 用户邮箱
	
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
