package cn.javaex.hgo.action.system.controller.hgo_user_role;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.javaex.hgo.action.system.service.hgo_user_info.HgoUserInfoService;
import cn.javaex.hgo.action.system.service.hgo_user_role.HgoUserRoleService;
import cn.javaex.hgo.action.system.view.HgoUserInfo;
import cn.javaex.hgo.config.exception.HgoException;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("hgo_user_role")
public class HgoUserRoleController {

	@Autowired
	private HgoUserRoleService hgoUserRoleService;
	@Autowired
	private HgoUserInfoService hgoUserInfoService;
	
	/**
	 * 为角色添加用户
	 * @param map
	 * @param roleId
	 * @return
	 */
	@RequestMapping("addUser.action")
	public String addUser(ModelMap map,
			@RequestParam(value="roleId") String roleId,
			@RequestParam(required=false, value="keyword") String keyword,
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize) {
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("roleId", roleId);
		map.put("roleId", roleId);
		
		if (StringUtils.isNotBlank(keyword)) {
			param.put("keyword", keyword.trim());
			map.put("keyword", keyword);
		}
		
		// pageHelper分页插件
		// 只需要在查询之前调用，传入当前页码，以及每一页显示多少条
		PageHelper.startPage(pageNum, pageSize);
		List<HgoUserInfo> list = hgoUserInfoService.list(param);
		PageInfo<HgoUserInfo> pageInfo = new PageInfo<HgoUserInfo>(list);
		map.put("pageInfo", pageInfo);
		
		return "system/hgo_role_info/add_user";
	}
	
	/**
	 * 向角色添加用户
	 * @param roleId 角色id
	 * @param userIdArr 用户id数组
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public Result save(
			@RequestParam(value="roleId") String roleId,
			@RequestParam(value="userIdArr") String[] userIdArr) throws HgoException {
		
		hgoUserRoleService.save(roleId, userIdArr);
		
		return Result.success();
	}
	
	/**
	 * 移除用户
	 * @param roleId 角色id
	 * @param userId 用户id
	 * @return
	 */
	@RequestMapping("deleteUser.json")
	@ResponseBody
	public Result deleteUser(
			@RequestParam(value="roleId") String roleId,
			@RequestParam(value="userId") String userId) {
		
		hgoUserRoleService.deleteUser(roleId, userId);
		
		return Result.success();
	}
	
	/**
	 * 批量移除用户
	 * @param roleId 角色id
	 * @param userIdArr 用户id数组
	 * @return
	 */
	@RequestMapping("batchDeleteUser.json")
	@ResponseBody
	public Result batchDeleteUser(
			@RequestParam(value="roleId") String roleId,
			@RequestParam(value="userIdArr") String[] userIdArr) {
		
		hgoUserRoleService.batchDeleteUser(roleId, userIdArr);
		
		return Result.success();
	}
}
