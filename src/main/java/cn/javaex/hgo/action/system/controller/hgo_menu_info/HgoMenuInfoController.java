package cn.javaex.hgo.action.system.controller.hgo_menu_info;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.javaex.hgo.action.system.service.hgo_menu_info.HgoMenuInfoService;
import cn.javaex.hgo.action.system.view.HgoMenuInfo;
import cn.javaex.hgo.config.exception.HgoException;
import cn.javaex.hgo.util.Result;

@Controller
@RequestMapping("hgo_menu_info")
public class HgoMenuInfoController {

	@Autowired
	private HgoMenuInfoService hgoMenuInfoService;
	
	/**
	 * 查询所有菜单
	 * @param map
	 * @return
	 */
	@RequestMapping("list.action")
	public String list(ModelMap map) {
		
		List<HgoMenuInfo> list = hgoMenuInfoService.list();
		map.put("list", list);
		
		return "system/hgo_menu_info/hgo_menu_info_list";
	}
	
	/**
	 * 添加菜单
	 * @param map
	 * @param id 菜单id
	 * @return
	 */
	@RequestMapping("addMenu.action")
	public String addMenu(ModelMap map,
			@RequestParam(required=false, value="id") String id) {
		
		if (StringUtils.isNotEmpty(id)) {
			HgoMenuInfo hgoMenuInfo = hgoMenuInfoService.selectById(id);
			map.put("hgoMenuInfo", hgoMenuInfo);
		}
		
		// 查询所有顶级菜单名称
		List<HgoMenuInfo> parentList = hgoMenuInfoService.listByParentId("-1");
		map.put("parentList", parentList);
		
		// 查询当前菜单级别下最大排序
		String parentId = "-1";
		if (StringUtils.isNotEmpty(id)) {
			parentId = id;
		}
		int maxSort = hgoMenuInfoService.selectMaxSortByParentId(parentId);
		map.put("maxSort", maxSort+1);
		
		return "system/hgo_menu_info/add_menu";
	}
	
	/**
	 * 菜单编辑
	 * @param map
	 * @param id 菜单id
	 * @return
	 */
	@RequestMapping("editMenu.action")
	public String editMenu(ModelMap map,
			@RequestParam(value="id") String id) {
		// 查询当前菜单信息
		HgoMenuInfo hgoMenuInfo = hgoMenuInfoService.selectById(id);
		map.put("hgoMenuInfo", hgoMenuInfo);
		
		// 查询所有顶级菜单名称
		List<HgoMenuInfo> parentList = hgoMenuInfoService.listByParentId("-1");
		map.put("parentList", parentList);
		
		return "system/hgo_menu_info/edit_menu";
	}
	
	/**
	 * 为当前菜单页面添加按钮
	 * @param map
	 * @param id 菜单id
	 * @return
	 */
	@RequestMapping("addButton.action")
	public String addButton(ModelMap map,
			@RequestParam(value="id") String id) {
		// 查询当前菜单信息
		HgoMenuInfo hgoMenuInfo = hgoMenuInfoService.selectById(id);
		map.put("hgoMenuInfo", hgoMenuInfo);
		
		// 查询当前菜单级别下最大排序
		int maxSort = hgoMenuInfoService.selectMaxSortByParentId(id);
		map.put("maxSort", maxSort+1);
		
		return "system/hgo_menu_info/add_button";
	}
	
	/**
	 * 编辑按钮权限
	 * @param map
	 * @param id 主键
	 * @return
	 */
	@RequestMapping("editButton.action")
	public String editButton(ModelMap map,
			@RequestParam(value="id") String id) {
		// 查询当前按钮信息
		HgoMenuInfo hgoButtonInfo = hgoMenuInfoService.selectById(id);
		map.put("hgoButtonInfo", hgoButtonInfo);
		
		// 查询当前按钮所对应的菜单信息
		HgoMenuInfo hgoMenuInfo = hgoMenuInfoService.selectById(hgoButtonInfo.getParentId());
		map.put("hgoMenuInfo", hgoMenuInfo);
		
		return "system/hgo_menu_info/edit_button";
	}
	
	/**
	 * 保存菜单
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public Result save(HgoMenuInfo hgoMenuInfo) throws HgoException {
		
		hgoMenuInfoService.save(hgoMenuInfo);
		
		return Result.success();
	}
	
	/**
	 * 删除菜单
	 * @param id 菜单id
	 * @return
	 * @throws HgoException
	 */
	@RequestMapping("delete.json")
	@ResponseBody
	public Result delete(@RequestParam(value="id") String id) throws HgoException {
		
		hgoMenuInfoService.deleteById(id);
		
		return Result.success();
	}
	
	/**
	 * 保存排序
	 * @param idArr
	 * @param sortArr
	 * @return
	 */
	@RequestMapping("saveSort.json")
	@ResponseBody
	public Result saveSort(
			@RequestParam(value="idArr") String[] idArr,
			@RequestParam(value="sortArr") int[] sortArr) {
		
		List<HgoMenuInfo> hgoMenuList = new ArrayList<HgoMenuInfo>();
		
		// 遍历idArr数组
		for (int i=0; i<idArr.length; i++) {
			HgoMenuInfo hgoMenuInfo = new HgoMenuInfo();
			hgoMenuInfo.setId(idArr[i]);
			hgoMenuInfo.setSort(sortArr[i]);
			
			hgoMenuList.add(hgoMenuInfo);
		}
		
		hgoMenuInfoService.saveSort(hgoMenuList);
		
		return Result.success();
	}
	
	/**
	 * 获取某个角色下的菜单列表
	 * @param roleId
	 * @return
	 */
	@RequestMapping("listMenuByRole.json")
	@ResponseBody
	public Result listMenuByRole(@RequestParam(value="roleId") String roleId) {
		
		List<HgoMenuInfo> list = hgoMenuInfoService.listByRoleId(roleId);
		
		return Result.success().add("list", list);
	}
}
