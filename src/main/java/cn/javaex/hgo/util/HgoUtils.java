package cn.javaex.hgo.util;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import cn.javaex.hgo.action.system.view.HgoUserInfo;
import cn.javaex.hgo.config.constant.HgoErrorMsg;
import cn.javaex.hgo.config.exception.HgoException;

/**
 * 系统工具类
 * 
 * @author 陈霓清
 */
public class HgoUtils {

	/**
	 * 获取当前 Subject
	 * @return
	 */
	public static Subject getSubject() {
		return SecurityUtils.getSubject();
	}
	
	/**
	 * 获取用户信息
	 * @return
	 * @throws HgoException
	 */
	public static HgoUserInfo getUserInfo() throws HgoException {
		Subject subject = SecurityUtils.getSubject();
		if (!subject.isAuthenticated()) {
			// 未认证，重新登录
			throw new HgoException(HgoErrorMsg.ERROR_100007);
		}
		
		return (HgoUserInfo)subject.getPrincipal();
	}
}
