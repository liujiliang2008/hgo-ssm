package cn.javaex.hgo.util;

/**
 * 格式化工具类
 * 
 * @author 陈霓清
 */
public class HgoStringUtils {
	
	private static final char SEPARATOR = '_';
	
	/**
	 * 格式化链接地址
	 * @param url
	 * @return
	 */
	public static String formatUrl(String url) {
		// 1.0 将url中的多个/置换为一个
		if (url.indexOf("//")>-1) {
			url = url.replace("//", "/");
			return formatUrl(url);
		}
		
		// 2.0 将url中的第一个/去除
		if (url.startsWith("/")) {
			url = url.substring(1, url.length());
		}
		return url;
	}
	
	/**
	 * 首字母转小写
	 * @param str
	 * @return
	 */
	public static String initLower(String str) {
		if (str!=null && str.isEmpty()==false) {
			str = str.substring(0, 1).toLowerCase() + str.substring(1);
		}
		
		return str;
	}
	
	/**
	 * 首字母转大写
	 * @param str
	 * @return
	 */
	public static String initCap(String str) {
		if (str!=null && str.isEmpty()==false) {
			str = str.substring(0, 1).toUpperCase() + str.substring(1);
		}
		
		return str;
	}
	
	/**
	 * 所有字母转小写，单词之间用下划线分隔
	 * @param str
	 * @return
	 */
	public static String toUnderlineName(String str) {
		if (str==null || str.isEmpty()) {
			return str;
		}
		
		StringBuilder sb = new StringBuilder();
		boolean upperCase = false;
		for (int i=0; i<str.length(); i++) {
			char c = str.charAt(i);
			
			boolean nextUpperCase = true;
			
			if (i<(str.length()-1)) {
				nextUpperCase = Character.isUpperCase(str.charAt(i + 1));
			}
			
			if ((i>=0) && Character.isUpperCase(c)) {
				if (!upperCase || !nextUpperCase) {
					if (i>0) {
						sb.append(SEPARATOR);
					}
				}
				upperCase = true;
			} else {
				upperCase = false;
			}
			
			sb.append(Character.toLowerCase(c));
		}

		return sb.toString();
	}
	
	/**
	 * 所有字母转小写，除第一个单词以外，后续的单词首字母转大写
	 * @param str
	 * @return
	 */
	public static String toCamelCase(String str) {
		if (str==null || str.isEmpty()) {
			return str;
		}
		
		str = str.toLowerCase();
		
		StringBuilder sb = new StringBuilder(str.length());
		boolean upperCase = false;
		for (int i=0; i<str.length(); i++) {
			char c = str.charAt(i);
			
			if (c==SEPARATOR) {
				upperCase = true;
			} else if (upperCase) {
				sb.append(Character.toUpperCase(c));
				upperCase = false;
			} else {
				sb.append(c);
			}
		}
		
		return sb.toString();
	}
	
	/**
	 * 所有字母转小写，单词首字母转大写
	 * @param str
	 * @return
	 */
	public static String toCapitalizeCamelCase(String str) {
		if (str==null || str.isEmpty()) {
			return str;
		}
		
		str = toCamelCase(str);
		return initCap(str);
	}
}
