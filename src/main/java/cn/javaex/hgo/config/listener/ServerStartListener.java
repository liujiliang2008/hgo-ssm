package cn.javaex.hgo.config.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import cn.javaex.hgo.config.constant.HgoConstant;

/***
 * 监听器
 * 
 * @author 陈霓清
 */
public class ServerStartListener implements ServletContextListener {

	/**
	 * web应用对象初始化的时候会被监听到
	 */
	public void contextInitialized(ServletContextEvent sce) {
		// 初始化将${pageContext.request.contextPath}设置为变量${domain}
		ServletContext application = sce.getServletContext();
		String domain = application.getContextPath();
		application.setAttribute("domain", domain);
		
		// 设置系统名称
		application.setAttribute("system_name", HgoConstant.SYSTEM_NAME);
	}
	
	public void contextDestroyed(ServletContextEvent sce) {
		
	}
}
