package cn.javaex.hgo.config.exception;

/**
 * 系统自定义异常类：抛出预期异常信息
 * 
 * @author 陈霓清
 */
public class HgoException extends Exception {

	private static final long serialVersionUID = 1L;
	
	// 异常信息
	public String message;
	
	public HgoException(String message){
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
