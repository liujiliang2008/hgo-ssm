package cn.javaex.hgo.config.constant;

/**
 * 固定选项类
 * 
 * @author 陈霓清
 */
public class HgoFixedOption {
	
	/**
	 * 表字段类型
	 * 
	 * @author 陈霓清
	 */
	public enum TableColumnType {
		INT("int", "整数（int）"),
		VARCHAR("varchar", "字符串（varchar）"),
		DOUBLE("double", "数值型（double）"),
		DATETIME("datetime", "日期时间（datetime）"),
		TINYINT("tinyint", "布尔型（tinyint）"),
		DECIMAL("decimal", "BigDecimal（decimal）"),
		DATE("date", "日期（date）"),
		TEXT("text", "文本（text）"),
		SMALLINT("smallint", "小整数（smallint）"),
		MEDIUMINT("mediumint", "中等大小整数（mediumint）")
		;
		
		private String value;
		private String name;
		
		private TableColumnType(String value, String name) {
			this.value = value;
			this.name = name;
		}

		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	
	public static void main(String[] args) {
		// 输出某一枚举的值
		System.out.println(TableColumnType.DOUBLE.getValue());
		System.out.println(TableColumnType.DOUBLE.getName());
		
		// 遍历所有的枚举
		for (TableColumnType opt : TableColumnType.values()) {
			System.out.println(opt + "		value:" + opt.getValue() + "  name:" + opt.getName());
		}
	}
}
