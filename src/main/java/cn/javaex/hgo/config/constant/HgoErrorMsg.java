package cn.javaex.hgo.config.constant;

/**
 * 错误信息
 * 
 * @author 陈霓清
 */
public class HgoErrorMsg {
	
	/** 系统繁忙，请稍后重试 */
	public static final String ERROR_999999 = "系统繁忙，请稍后重试";
	/** 权限不足，请联系管理员 */
	public static final String ERROR_403 = "权限不足，请联系管理员";
	
	// 权限配置相关
	// 用户相关
	/** 账号或密码不能为空 */
	public static final String ERROR_100001 = "账号或密码不能为空";
	/** 账号已被占用 */
	public static final String ERROR_100002 = "账号已被占用";
	/** 密码不能为空 */
	public static final String ERROR_100003 = "密码不能为空";
	/** 密码长度应为6到16个字符 */
	public static final String ERROR_100004 = "密码长度应为6到16个字符";
	/** 账号或密码错误 */
	public static final String ERROR_100005 = "账号或密码错误";
	/** 该用户已分配角色，无法删除 */
	public static final String ERROR_100006 = "该用户已分配角色，无法删除";
	/** 登录超时，请重新登录 */
	public static final String ERROR_100007 = "登录超时，请重新登录";
	/** 账号已被封禁，禁止登录 */
	public static final String ERROR_100008 = "账号已被封禁，禁止登录";
	/** 您的账号已在另一台设备登录，如非您本人操作，请立即修改密码 */
	public static final String ERROR_100009 = "您的账号已在另一台设备登录，如非您本人操作，请立即修改密码";
	// 菜单
	/** 按钮权限标识重复 */
	public static final String ERROR_100010 = "按钮权限标识重复";
	// 角色
	/** 角色标识重复 */
	public static final String ERROR_100011 = "角色标识重复";
	// 用户自己修改个人信息
	/** 原密码不正确 */
	public static final String ERROR_100012 = "原密码不正确";
	
	// 表单管理
	/** 表名重复 */
	public static final String ERROR_200001 = "表名重复";
	/** 字段名重复 */
	public static final String ERROR_200002 = "字段名重复";
	/** 至少选择一种生成类型 */
	public static final String ERROR_200003 = "至少选择一种生成类型";
	
	// 邮件相关
	/** 您的邮箱已满，请清理后重试 */
	public static final String ERROR_300001 = "您的邮箱已满，请清理后重试";
	/** 您的邮件账号可能输入错误 */
	public static final String ERROR_300002 = "您的邮件账号可能输入错误";
	/** 发信人的身份认证失败 */
	public static final String ERROR_300003 = "发信人的身份认证失败";
	/** 您的信箱大小受到限制 */
	public static final String ERROR_300004 = "您的信箱大小受到限制";
	/** 发信人的身份认证失败 */
	public static final String ERROR_300005 = "发信人的身份认证失败";
	/** 系统异常，请稍后重试 */
	public static final String ERROR_300006 = "系统异常，请稍后重试";
	/** 该网站未开启邮件发送服务 */
	public static final String ERROR_300007 = "该网站未开启邮件发送服务";
	
}
