package cn.javaex.hgo.config.constant;

/**
 * 系统常量
 * 
 * @author 陈霓清
 */
public class HgoConstant {
	
	/** 系统名称：将会在登录页、左上角、页面title显示 */
	public static final String SYSTEM_NAME = "Hgo开发脚手架";
	
	/** 邮件发件人：可以在后台业务中自己指定发件人。如果没指定，此处亦未设置的话，则会将邮件配置中的邮箱作为发件人 */
	public static final String EMAIL_SENDER = "";
	
	/** 代码生成模板路径 */
	public static final String GENERATE_TEMPLATE = "WEB-INF\\classes\\generateTemplate";
	
	/** 代码生成jsp路径 */
	public static final String GENERATE_JSP = "src/main/webapp/WEB-INF/page/";
}
