package cn.javaex.hgo.config.druid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/alibaba")
public class Druid {
	
	@RequestMapping("druid.action")
	public String druid() {
		return "system/druid/druid";
	}
}